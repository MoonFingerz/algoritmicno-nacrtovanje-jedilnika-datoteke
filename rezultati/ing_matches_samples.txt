From	To	Matches	110	105	69	16
watercress	"watercress, raw"	1		10		
pretzels	yeast extract spread	0				
almond extract	vanilla extract	0.5		0.61452514	0.564102564	
package cope corn corn	"oil, corn and canola"	0				
onion pieces	"onions, raw"	1				
lobster tail lengthwise crosswise shrimp lengthwise	"spices, anise seed"	0				
shiitake mushrooms caps cups	"mushrooms, shiitake, dried"	1				
package firm tofu	"beverages, orange juice drink"	0				
firm bosc pears cubes cups	"spices, cumin seed"	0				
ground chocolate almond coffee beans	"candies, semisweet chocolate"	0				
pumpkin seeds	"seeds, pumpkin and squash seed kernels, dried"	1				
goose neck	"rosemary, fresh"	0				
bags greens	"beans, snap, green, microwaved"	1				
package cream cheese room temperature	"cream, fluid, light (coffee cream or table cream)"	1				
firm tofu water	"watercress, raw"	0				
halves	olives	-1				
blood oranges navel oranges	"marmalade, orange"	0				
pound inch pieces	"ground turkey, raw"	-1				
ginger ounces	"ginger root, raw"	1				
oregano leaves	"spices, oregano, dried"	1				
cinnamon sticks	"spices, cinnamon, ground"	1				
brine olives	olives	1				
piment d'espelette teaspoon pepper	"spices, pepper, black"	1				
pork chops	"smoked link sausage, pork"	1				
fryer pieces	"spices, coriander seed"	-1				
horseradish horseradish	"horseradish, prepared"	1				
apples half	"cream, fluid, half and half"	0				
tomatoes puree	"tomatoes, red, ripe, raw, year round average"	1				
hothouse cucumber	"cucumber, peeled, raw"	1				
halves cups	olives	-1				
inch thick skinless sturgeon fillet halibut fillet	vegetarian fillets	0				
chiles	"sauce, hot chile, sriracha"	1				
ice water	"watercress, raw"	0				
baby greens	"beans, snap, green, microwaved"	1				
cans beans	"nuts, pecans"	0				
peel	"lemon peel, raw"	1				
cooking grits	"oil, pam cooking spray, original"	0				
bean soup	"cream, sour, cultured"	0				
citrus vinaigrette	"spaghetti, spinach, cooked"	0				
cilantro tender stems	"coriander (cilantro) leaves, raw"	1				
chopped tarragon parsley	"spices, tarragon, dried"	1				
plain biscotti	"bread crumbs, dry, grated, plain"	0				
boneless pork loin	"veal, leg, top round, cap off, cutlet, boneless, cooked, grilled"	1				
salmon	"nuts, almonds"	0				
lentils	"tomatillos, raw"	0				
tablespoons butter pieces	"butter, salted"	1				
lard butter	"butter, salted"	1				
raki ouzo anise liqueur	"spices, anise seed"	0				
skinless boneless breasts	"bacon, meatless"	0				
firm bread	"denny's, fish fillet, battered or breaded, fried"	0				
inch thick brioche challah inch loaf	oven-roasted chicken breast roll	0				
ounces sugar	"sugars, brown"	1				
raspberries ounces	"blueberries, raw"	0.5				
confectioners sugar	"sugars, brown"	1				
mission figs	"figs, raw"	1				
romaine	"lettuce, cos or romaine, raw"	1				
skinless boneless breast lb total	oven-roasted chicken breast roll	1				
mild molasses	"mollusks, mussel, blue, raw"	0				
bunches asparagus inches	"peaches, yellow, raw"	0				
water	"watercress, raw"	0				
tequila	"coriander (cilantro) leaves, raw"	0				
crumb	"bread crumbs, dry, grated, plain"	1				
oz bag watercress stems	"watercress, raw"	1				
head cauliflower lengthwise wedges	"leeks, (bulb and lower leaf-portion), raw"	0				
bag turnip mustard collard greens stems cups	"mustard, prepared, yellow"	-1				
peaches pounds	"peaches, yellow, raw"	1				
quality chocolate	"candies, semisweet chocolate"	1				
sweet potatoes	"potatoes, raw, skin"	1				
bunch cilantro roots	"coriander (cilantro) leaves, raw"	1				
breadcrumbs*	"bread crumbs, dry, grated, plain"	1				
size	"cream, whipped, cream topping, pressurized"	-1				
range chickens	"orange peel, raw"	0				
turkey neck heart	"ground turkey, raw"	1				
potatoes inch pieces	"potatoes, raw, skin"	1				
boneless breast skin wings pounds	"apples, raw, without skin"	0				
chilies	"spices, chili powder"	1				
rutabaga inch pieces	"rutabagas, raw"	1				
apple holes grater	"cheese, parmesan, grated"	0				
parsnips inch pieces	"parsnips, raw"	1				
shoulder bone	honey	0				
blackberries	"blueberries, raw"	0.5				
oil fillets	vegetarian fillets	0				
flour	"wheat flour, white, all-purpose, enriched, bleached"	1				
cans chickpeas	oven-roasted chicken breast roll	0				
mayonnaise	"mayonnaise, reduced fat, with olive oil"	1				
milk chocolate chips ounces	"candies, semisweet chocolate"	1				
shoulder bone round bone chops inch pieces	"veal, leg, top round, cap off, cutlet, boneless, cooked, grilled"	1				
teaspoon oil	"oil, canola"	1				
wasabi powder	"spices, chili powder"	0.5				
shanks total	"shallots, raw"	0				
dijon mustard	"mustard, prepared, yellow"	1				
semisweet bittersweet chocolate % cacao	"candies, semisweet chocolate"	1				
serrano chile seeds	"peppers, serrano, raw"	0				
semi boneless	"veal, leg, top round, cap off, cutlet, boneless, cooked, grilled"	-1				
ripe peaches	"peaches, yellow, raw"	1				
butter room temperature	"butter, salted"	1				
zucchini	"chives, raw"	0				
baby racks	"babyfood, juice, apple"	0				
artichokes	"pasta, cooked, enriched, with added salt"	0				
capocollo pancetta strips	"candies, semisweet chocolate"	0				
bars bittersweet chocolate chunks	"candies, semisweet chocolate"	1				
thyme teaspoon	"thyme, fresh"	1				
wine vinegar vinegar	"vinegar, red wine"	1				
kalamata olives	olives	1				
bread	"bread crumbs, dry, grated, plain"	1				
granny smith apple	"babyfood, juice, apple"	0				
stout beer room temperature	"ginger root, raw"	0				
self	"mollusks, mussel, blue, raw"	-1				
firm pears thick slices	"spices, pepper, black"	0				
ounces walnuts	"nuts, walnuts, english"	1				
package corn kernels	"corn, yellow, whole kernel, frozen, microwaved"	1				
basil	"basil, fresh"	1				
inch corn	"syrups, corn, light"	1				
cups/400 herbs greens pieces	"beans, snap, green, microwaved"	-1				
spinach	"spinach, raw"	1				
tablespoons madeira	"salt, table, kosher"	0				
pure almond extract	vanilla extract	0.5				
corn kernels	"corn, yellow, whole kernel, frozen, microwaved"	1				
goat cheese montrachet slices	"cheese, feta"	1				
shanks	"shallots, raw"	0				
sodium chicken broth	"soup, chicken broth, ready-to-serve"	1				
ground pork	"ground turkey, raw"	0.5				
bay scallops	"mollusks, scallop, mixed species, raw"	0				
walnuts cup tablespoons	"nuts, walnuts, english"	1				
plum tomatoes	"tomatoes, red, ripe, raw, year round average"	1				
frying	"leavening agents, baking soda"	-1				
bunch medium bunches watercress cups	"watercress, raw"	1				
jar apricot preserves	"apricots, raw"	1				
center cut bone pork loin rib roast	oven-roasted chicken breast roll	0				
garam masala	"spices, marjoram, dried"	0				
onion lengthwise	"onions, raw"	1				
lemons	"lemons, raw, without peel"	1				
package spinach	"spinach, raw"	1				
chopped tarragon	"spices, tarragon, dried"	1				
mesclun cups	"spices, cumin seed"	0				
ham	"pork, fresh, leg (ham), whole, separable lean only, raw"	1				
clams	"beverages, clam and tomato juice, canned"	0				
poussins game hens	"raisins, seeded"	0				
half ounce package room temperature	"cream, fluid, half and half"	-1				
flowerets head	"leeks, (bulb and lower leaf-portion), raw"	-1				
granny smith apples pounds inch cubes	"apples, raw, without skin"	1				
leg lamb fat chunks	"beef, chuck, clod roast, separable lean and fat, trimmed to 0"" fat, choice, cooked, roasted"	0				
scallops	"mollusks, scallop, mixed species, raw"	1				
shells pound shrimp	"ground turkey, raw"	0				
bell peppers pieces	"spices, pepper, black"	0				
apples cubes cups	"apples, raw, without skin"	1				
orange wheel	"orange peel, raw"	1				
cold rice	"rice, white, glutinous, unenriched, cooked"	1				
macadamia nuts	"nuts, cashew nuts, raw"	1				
butter inch cubes	"butter, salted"	1				
salt chicken broth	"soup, chicken broth, ready-to-serve"	1				
cherries currants	"cherries, sweet, raw"	1				
goat milk cow milk	"milk, whole, 3.25% milkfat, with added vitamin d"	1				
corn oil	"oil, corn and canola"	1				
pork tenderloins	"beef, tenderloin, separable lean and fat, trimmed to 1/8"" fat, prime, raw"	0.5				
pork cutlets	"veal, leg, top round, cap off, cutlet, boneless, cooked, grilled"	0.5				
cranberries ounces	"cranberries, raw"	1				
skin potatoes	"potatoes, raw, skin"	1				
bourbon	"beverages, carbonated beverage, club soda"	0				
tomatoes cups	"tomatoes, red, ripe, raw, year round average"	1				
cod fillets	vegetarian fillets	0				
lime	"lime juice, raw"	1				
avocados	"oil, olive, salad or cooking"	0				
inch thick bone pork chops lb total	"smoked link sausage, pork"	0.5				
brown sugar	"sugars, brown"	1				
bone ham steak inch thick	"denny's, top sirloin steak"	0				
tablespoon oil	"salt, table, kosher"	0				
jasmine rice grain rice	"spices, turmeric, ground"	0				
steak	"denny's, top sirloin steak"	1				
earl grey tea	"beans, snap, green, microwaved"	0				
blade beef inch thick	"alcoholic beverage, beer, light"	-1				
cup tablespoons butter	"butter, salted"	1				
ground turmeric	"spices, turmeric, ground"	1				
grind cornmeal	"oil, corn and canola"	0				
plain breadcrumbs	"bread crumbs, dry, grated, plain"	1				
inch ounces	"yogurt, plain, whole milk, 8 grams protein per 8 ounce"	-1				
celery stalk	"celery, raw"	1				
tomato dice	"beverages, clam and tomato juice, canned"	0				
honey	honey	1				
apricot preserves	"apricots, raw"	1				
container cream cheese	"cream, fluid, light (coffee cream or table cream)"	0				
butternut squash cups	"butter, salted"	0				
dark molasses	"mollusks, mussel, blue, raw"	0				
coarse grain mustard	"mustard, prepared, yellow"	1				
packages baby lima beans	"beans, baked, home prepared"	1				
sriracha sauce	"sauce, hot chile, sriracha"	1				
chopped pecans	"nuts, pecans"	1				
quality chocolate lindt perugina	"candies, semisweet chocolate"	1				
scallion greens	"onions, spring or scallions (includes tops and bulb), raw"	1				
thyme tablespoon	"salt, table, kosher"	0				
peppers jalape�os pounds	"peppers, serrano, raw"	1				
cinnamon	"spices, cinnamon, ground"	1				
inch cinnamon	"spices, cinnamon, ground"	1				
fatback	"pork, bacon, rendered fat, cooked"	-1				
inch round	"veal, ground, raw"	-1				
coarse mustard	"mustard, prepared, yellow"	1				
inch quality bread pain pullman sourdough crusts	"bread crumbs, dry, grated, plain"	0				
size corn tortillas	"oil, corn and canola"	0				
country style bread crust pieces cups	"bread crumbs, dry, grated, plain"	0.5				
shallots	"shallots, raw"	1				
