library(ggplot2)
library("reshape2")
library(gganimate)
library(tidyverse)
library(Cairo)
library(ggrepel)
library(gsubfn)

x_title_margin = margin(t = 5, r = 0, b = 0, l = 0)
y_title_margin = margin(t=0, r=4, b=0, l = 0)

calcAvgSd <- function(db) {
  return(c(mean(db), sd(db)));
}

calcAvgSdNutrients <- function(db, n) {
  nuts = c("fat", "carbohydrate", "protein", "sodium", "sugars", "calories")
  avg_nut = c()
  sd_nut = c()
  
  for (nut in nuts){
    avg_nut = c(avg_nut, mean(db[1:n, nut]))
    sd_nut = c(sd_nut, sd(db[1:n, nut]))
  }
  
  return(list(avg_nut, sd_nut));
}

calcScores <- function(db, n) {
  scores = c()
  prob_nut <- calcAvgSd(db$nut_score[1:n])
  scores = c(scores, prob_nut)
  prob_pref <- calcAvgSd(db$pref_score[1:n])
  scores = c(scores, prob_pref)
  prob_hetero <- calcAvgSd(db$hetero_score[1:n])
  scores = c(scores, prob_hetero)
  prob_div <- calcAvgSd(db$div_score[1:n])
  scores = c(scores, prob_div)
  
  return(scores)
}

calcTime <- function(db){
  return(c(mean(db$time), sd(db$time)))
}

setwd('D:/Masters/meal-planner-masters/r_visualisation')
#setwd('C:/Users/aalen/Desktop/magistrska/meal-planner-masters/r_visualisation')

plots_path <- 'D:/Masters/thesis/figures/results(R)_priporocevalci/'
#plots_path <- 'C:/Users/aalen/Desktop/magistrska/thesis/figures/results(R)_priporocevalci'

rec <- c(78, 275, 50, 2300, 50, 2000)

#normalised_all and equivalence
normalised_F <- read.csv(file = file.path("recommenders", "matrix_scores_all_normalised_filtering_False.csv"))
normalised_T <- read.csv(file = file.path("recommenders", "matrix_scores_all_normalised_filtering_True.csv"))

equiv_class_F <- read.csv(file = file.path("recommenders", "matrix_scores_equiv_class_filtering_False.csv"))
equiv_class_T <- read.csv(file = file.path("recommenders", "matrix_scores_equiv_class_filtering_True.csv"))

scores_norm_F <- calcScores(normalised_F, 100)
scores_norm_T <- calcScores(normalised_T, 100)
scores_eq_F <- calcScores(equiv_class_F, 100)
scores_eq_T <- calcScores(equiv_class_T, 100)

specie <- c(rep("vsi_NF" , 4) , rep("vsi_F" , 4) , rep("ekv_NF" , 4) , rep("ekv_F",4))
condition <- rep(c("hranil" , "pred." , "hetero.", "razno.") , 4)
value <- c(scores_norm_F[c(TRUE,FALSE)], scores_norm_T[c(TRUE, FALSE)], scores_eq_F[c(TRUE,FALSE)], scores_eq_T[c(TRUE,FALSE)])
#value[value<0.1] <- NA
data <- data.frame(specie,condition,value)

ggplot(data, aes(fill=condition, y=value, x=factor(specie, c("vsi_NF", "vsi_F", "ekv_NF", "ekv_F")), width=0.8)) + 
  theme(legend.title = element_text(size=18), legend.text = element_text(size=18), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=26), text = element_text(size=20), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin)) +
  labs(x="Nastavitve matri\u010dne faktorizacije", y="Skupna ocena", fill="Ocena") +
  geom_bar(position="stack", stat="identity") +
  geom_text(size = 7, position = position_stack(vjust = 0.5), label=ifelse(value > 0.1, sprintf("%0.2f", round(value, digits = 2)), "")) +
  geom_text(size=8,
            aes(label = sprintf("%0.2f", round(stat(y), digits=2)), group = specie), 
            stat = 'summary', fun = sum, vjust = -1
  ) + ylim(c(0,2.4))

ggsave("NMF_comparison.pdf", path=plots_path, device=cairo_pdf)

#dessert
dessert_F <- read.csv(file = file.path("recommenders", "matrix_scores_dessert_equiv_filtering_False.csv"))
dessert_T <- read.csv(file = file.path("recommenders", "matrix_scores_dessert_equiv_filtering_True.csv"))

dessert_red_ing_F <- read.csv(file = file.path("recommenders", "matrix_scores_dessert_equiv_reduced_ing_filtering_False.csv"))
dessert_red_ing_T <- read.csv(file = file.path("recommenders", "matrix_scores_dessert_equiv_reduced_ing_filtering_True.csv"))

des_F <- calcScores(dessert_F, 100)
des_T <- calcScores(dessert_T, 100)
des_red_F <- calcScores(dessert_red_ing_F, 100)
des_red_T <- calcScores(dessert_red_ing_T, 100)

specie <- c(rep("slad_F" , 4) , rep("slad_NF" , 4) , rep("slad_manj_F" , 4) , rep("slad_manj_NF",4))
condition <- rep(c("hranil" , "pred." , "hetero.", "razno.") , 4)
value <- c(des_F[c(TRUE,FALSE)], des_T[c(TRUE, FALSE)], des_red_F[c(TRUE,FALSE)], des_red_T[c(TRUE,FALSE)])
#value[value<0.1] <- NA
data <- data.frame(specie,condition,value)

ggplot(data, aes(fill=condition, y=value, x=factor(specie, c("slad_F", "slad_NF", "slad_manj_F", "slad_manj_NF")), width=0.8)) + 
  theme(legend.title = element_text(size=18), legend.text = element_text(size=18), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=26), text = element_text(size=20), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin)) +
  labs(x="Nastavitve matri\u010dne faktorizacije", y="Skupna ocena", fill="Ocena") +
  geom_bar(position="stack", stat="identity") +
  geom_text(size = 7, position = position_stack(vjust = 0.5), label=sprintf("%0.2f", round(value, digits = 2))) +
  geom_text(size=8,
            aes(label = sprintf("%0.2f", round(stat(y), digits=2)), group = specie), 
            stat = 'summary', fun = sum, vjust = -1
  ) + ylim(c(0,2.7))

ggsave("NMF_des_comparison.pdf", path=plots_path, device=cairo_pdf)

#time graph
time_all <- read.csv(file = file.path("recommenders", "time_matrix_normalised.csv"))
time_equiv <- read.csv(file = file.path("recommenders", "time_matrix_equiv_class.csv"))
time_des <- read.csv(file = file.path("recommenders", "time_matrix_dessert_equiv_class.csv"))
time_des_red <- read.csv(file = file.path("recommenders", "time_matrix_dessert_equiv_class_reducedIngredientSet.csv"))

t_all <- calcTime(time_all)
t_equiv <- calcTime(time_equiv)
t_des <- calcTime(time_des)
t_des_red <- calcTime(time_des_red)

mat <- c("vsi", "ekviv_razredi", "sladice", "sladice_manj")
time <- c(t_all[1], t_equiv[1], t_des[1], t_des_red[1])
time_data <- data.frame(mat, time)

ggplot(time_data, aes(x=factor(mat, mat), y=time, width=0.6)) +
  geom_bar(stat="identity", fill="#619CFF") + labs(x="Vrsta uporabljene matrike", y="Pretekel \u010das (s)") + 
  geom_text(aes(label = sprintf("%.2f", time), hjust = +0.5, vjust = -0.5), size = 8) +
  theme(legend.title = element_text(size=20), legend.text = element_text(size=26), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=22), text = element_text(size=22), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin)) +
  ylim(c(0,52))

ggsave("NMF_time.pdf", path=plots_path, device=cairo_pdf)

#k time graph
k = c(1,2,3,4,5,6,7,8,9,10,20,30,50,100,250,500)
k = paste(k)
time = c(25.0543942, 30.0191337, 32.21209, 28.6484448, 30.08847, 30.8073346, 33.1530577, 33.0254115, 33.6593428, 30.7344224, 29.8999031, 34.4718012, 32.4428747, 41.2814545, 77.7070145, 126.409633)
time = round(time, digits=1)

k_time_data <- data.frame(k, time)

ggplot(k_time_data, aes(x=factor(k,k), y=time)) + 
  geom_line(color="black", linetype="dashed",alpha=0.3) +
  geom_point(size=3)+
  geom_text(aes(label=time),vjust=-1.1,hjust=0.5,size=5)+
  expand_limits(x=12)+
  theme(legend.title = element_text(size=14), legend.text = element_text(size=14), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=26), text = element_text(size=20), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin)) +
  labs(x="Velikost faktorizacije (k)", y="Prete\u010den \u010das (s)", color="", size="") +
  geom_hline(yintercept=50, linetype="dashed", size=1, alpha=0.4) +
  geom_hline(yintercept=75, linetype="dashed", size=1, alpha=0.4) +
  geom_hline(yintercept=100, linetype="dashed", size=1, alpha=0.4) +
  geom_hline(yintercept=125, linetype="dashed", size=1, alpha=0.4) +
  ylim(c(25,130))

ggsave("NMF_k_time.pdf", path=plots_path, device=cairo_pdf)
#ggplot(data=k_time_data, aes(x=k, y=time)) +
#  geom_line() + geom_point()



#k score graph
k = c(1,2,3,4,5,6,7,8,9,10,20,30,50,100,250,500)
k_fac = c("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "20", "30", "50", "100", "250", "500")
specie <- c(rep("1", 4) , rep("2" , 4) , rep("3" , 4) , rep("4",4), rep("5",4), rep("6",4), rep("7",4), rep("8",4), rep("9",4), rep("10",4),rep("20",4),rep("30",4),rep("50",4),rep("100",4),rep("250",4),rep("500",4))
condition <- rep(c("hranil" , "pred." , "hetero.", "razno.") , 16)
values <- c()

for(i in k){
  data_k <- read.csv(file = file.path("recommenders", paste("matrix_scores_all_normalised_filtering_True_k_",toString(i),".csv",sep="")))
  scores_k <- calcScores(data_k, 100)
  values <- c(values, scores_k[c(TRUE,FALSE)])
  print(scores_k[c(TRUE,FALSE)])
}

k_data <- data.frame(specie, condition, values)

ggplot(k_data, aes(fill=condition, y=values, x=factor(specie, k_fac))) + geom_bar(position="stack", stat="identity") +
  theme(legend.title = element_text(size=22), legend.text = element_text(size=22), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=26), text = element_text(size=18), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin), axis.text.x=element_text(angle=0, hjust=0.4))+
  labs(x="Velikost faktorizacije (k)", y="Skupna ocena", fill="Ocena")


ggsave("NMF_k.pdf", path=plots_path, device=cairo_pdf)
