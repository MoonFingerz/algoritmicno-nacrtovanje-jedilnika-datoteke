library(ggplot2)
library("reshape2")
library(gganimate)
library(tidyverse)
library(Cairo)
library(ggrepel)
library(gsubfn)

x_title_margin = margin(t = 5, r = 0, b = 0, l = 0)
y_title_margin = margin(t=0, r=4, b=0, l = 0)

setwd('D:/Masters/meal-planner-masters/r_visualisation')
#setwd('C:/Users/aalen/Desktop/magistrska/meal-planner-masters/r_visualisation')

plots_path <- 'D:/Masters/thesis/figures/results(R)_priporocevalci/'
#plots_path <- 'C:/Users/aalen/Desktop/magistrska/thesis/figures/results(R)_priporocevalci'

######################## primerjava casa
time <- c(0.007,31.22,6.74,191.15)
nac <- c("Naklju\u010dni", "Verjetnostni", "NNMF", "Genetski")

nac_time <- data.frame(time, nac)

ggplot(nac_time, aes(x=factor(nac, nac), y=time, width=0.6)) +
  geom_bar(stat="identity", fill="#619CFF") + labs(x="Tip na\u010drtovalca", y="Pretekel \u010das (s)") + 
  geom_text(aes(label = sprintf("%.2f", time), hjust = +0.5, vjust = -0.5), size = 8) +
  theme(legend.title = element_text(size=14), legend.text = element_text(size=14), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=26), text = element_text(size=20), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin)) +
  ylim(c(0,200))

ggsave("all_time_comparison.pdf", path=plots_path, device=cairo_pdf)

######################## primerjava ocen
scores_title = c("nut_score", "pref_score", "hetero_score", "div_score")

scores_rand = c(0.45, 0.57, 0.88, 1) #random
scores_prob = c(0.50, 0.85, 0.74, 0.88) #rel, mul_pref=5
scores_nnmf = c(0.42, 0.63, 0.78, 0.71) #NF_sladice
scores_gen = c(0.92, 1, 0.90, 1) #(3,1,1)

specie <- c(rep("Naklju\u010dni" , 4) , rep("Verjetnostni" , 4) , rep("NNMF", 4), rep("Genetski", 4))
condition <- rep(c("hranil" , "pred." , "hetero.", "razno.") , 4)
values <- c(scores_rand, scores_prob, scores_nnmf, scores_gen)
data <- data.frame(specie,condition,values)

ggplot(data, aes(fill=condition, y=values, x=factor(specie, c("Naklju\u010dni", "Verjetnostni", "NNMF", "Genetski")))) + geom_bar(position="stack", stat="identity") +
  theme(legend.title = element_text(size=16), legend.text = element_text(size=18), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=26), text = element_text(size=20), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin), axis.text.x=element_text(angle=0, hjust=0.4))+
  labs(x="Tip na\u010drtovalca", y="Skupna ocena", fill="Ocena") +
  geom_text(size = 7, position = position_stack(vjust = 0.5), label=sprintf("%0.2f", round(values, digits = 2))) +
  geom_text(size=8,
            aes(label = sprintf("%0.2f", round(stat(y), digits=2)), group = specie), 
            stat = 'summary', fun = sum, vjust = -1
  ) + ylim(c(0,4))

ggsave("all_score_comparison.pdf", path=plots_path, device=cairo_pdf)
