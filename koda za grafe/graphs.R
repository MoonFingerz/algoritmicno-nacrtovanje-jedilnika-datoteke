library(ggplot2)
library("reshape2")
library(gganimate)
library(tidyverse)
library(Cairo)
library(ggrepel)

setwd('D:/Masters/meal-planner-masters/r_visualisation')
setwd('C:/Users/aalen/Desktop/magistrska/meal-planner-masters/r_visualisation')

plots_path <- 'D:/Masters/thesis/figures/results(R)/'
x_title_margin = margin(t = 5, r = 0, b = 0, l = 0)
y_title_margin = margin(t=0, r=2, b=0, l = 0)


data_match_errors_base <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_prod_n3.csv"))
ggplot(data_match_errors_base, aes(x=i, y=MAE))+geom_line(color="black")+xlab("indeks")+ylab("Povp. abs. napaka")

###error comparison with other settings as production (n=1, n=3, n=5, n=whole words), success rate
data_match_errors_n1 <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_prod_n1.csv"))
data_match_errors_n2 <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_prod_n2.csv"))
data_match_errors_n3 <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_prod_n3.csv"))
data_match_errors_n5 <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_prod_n5.csv"))
data_match_errors_n7 <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_prod_n7.csv"))
data_match_errors_whole <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_whole_words.csv"))

data_match_errors = setNames(data.frame(matrix(ncol = 5, nrow = 2001)), c("i","n=3", "n=5", "n=7", "besede"))
data_match_errors$i <- data_match_errors_n3$i
data_match_errors$`n=3` <- data_match_errors_n3$MAE
data_match_errors$`n=5` <- data_match_errors_n5$MAE
data_match_errors$`n=7` <- data_match_errors_n7$MAE
data_match_errors$besede <- data_match_errors_whole$MAE

data_match_errors_plot <- melt(data_match_errors, id="i")
data_match_errors_plot$thick <- rep("0",2001)
mask <- data_match_errors_plot$variable == "n=3"
data_match_errors_plot$thick[mask] = "1"

match_err_plot = ggplot(data=data_match_errors_plot,
       aes(x=i,y=value,colour=variable, size=thick)) + geom_line() + labs(x="St. zaporednega recepta", y="Kumulativna povp. abs. napaka", color="", size="") + scale_size_manual(values = c("1" = 1.3, "0" = 0.5), guide='none') + theme(legend.title = element_text(size=14), legend.text = element_text(size=14), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=16)) +
  theme(legend.title = element_text(size=18), legend.text = element_text(size=22), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=22), text = element_text(size=22), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin))

match_err_plot
#ggplot(data_match_errors, aes(x=i)) + geom_line(aes(y=`n=3`), color="red") + geom_line(aes(y=`besede`), color="blue") + xlab("indeks") + ylab("Povp. abs. napaka") + theme(legend.position = c(0.95, 0.95))
ggsave("plot_match_errors_n.pdf", path=plots_path, plot=match_err_plot, device=cairo_pdf)


###error with different bucket sizes (n)
error_bucket_size = setNames(data.frame(matrix(ncol = 3, nrow = 11)), c("n", "MAE"))
error_bucket_size$n = c("n=1", "n=2", "n=3", "n=4", "n=5", "n=6", "n=7", "n=8", "n=9", "n=10", "besede")
error_bucket_size$MAE=c(114.8, 114.9, 114.9, 106.7, 106.6, 106.7, 91.4, 105.8, 117.3, 124.9, 109.7)
error_bucket_size$grp=c("n1","n2","n3","n4","n5","n6","n7","n8","n9","n10","besede")

error_bucket_plot = ggplot(error_bucket_size, aes(x=factor(n, c("n=1", "n=2", "n=3","n=4","n=5","n=6","n=7","n=8","n=9","n=10","besede")), y=MAE, group=grp)) + 
  geom_line(color="black", linetype="dashed",alpha=0.3) +
  geom_point(size=3)+
  geom_text(aes(label=MAE),vjust=-1.1,hjust=0.5,size=5.5)+
  #geom_text(aes(label=n),vjust=1.7,hjust=0.5,size=4)+
  expand_limits(x=12)+
  theme(legend.title = element_text(size=20), legend.text = element_text(size=20), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=26), text = element_text(size=20), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin)) +
  labs(x="Velikost kosa", y="Povp. abs. napaka", color="", size="") +
  geom_hline(yintercept=100, linetype="dashed", size=1, alpha=0.4) +
  geom_hline(yintercept=110, linetype="dashed", size=1, alpha=0.4) +
  geom_hline(yintercept=120, linetype="dashed", size=1, alpha=0.4) +
  ylim(c(90,130))

error_bucket_plot

#error_bucket_plot = ggplot(error_bucket_size, aes(x=factor(n, c("n=1", "n=2", "n=3", "n=4", "n=5", "n=6", "n=7", "n=8", "n=9", "n=10", "besede")), y=MAE, width=0.6)) +
#  geom_bar(stat="identity", fill="#619CFF") + labs(x="Velikost kosa", y="Povp. abs. napaka") + 
# geom_text(aes(label = sprintf("%.2f", MAE), hjust = +0.5, vjust = -0.5), size = 4) +
#  theme(legend.title = element_text(size=14), legend.text = element_text(size=14), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=16), text = element_text(size=16))


error_bucket_plot
#error_bucket_plot

ggsave("plot_error_bucket.pdf", path=plots_path, plot=error_bucket_plot, device=cairo_pdf)


###time comparison with other settings as production (n=1, n=3, n=5, n=whole words), success rate
data_match_time = setNames(data.frame(matrix(ncol = 3, nrow = 4)), c("i", "ime", "cas"))
data_match_time$i = c(1:4)
data_match_time$ime = c("n=3", "n=5", "n=7", "besede")
casi = c()
#casi = c(casi, tail(data_match_errors_n1$elapsed_time, 1))
#casi = c(casi, tail(data_match_errors_n2$elapsed_time, 1))
casi = c(casi, tail(data_match_errors_n3$elapsed_time, 1))
casi = c(casi, tail(data_match_errors_n5$elapsed_time, 1))
casi = c(casi, tail(data_match_errors_n7$elapsed_time, 1))
casi = c(casi, tail(data_match_errors_whole$elapsed_time, 1))
data_match_time$cas = casi

match_time_plot = ggplot(data_match_time, aes(x=factor(ime, c("n=3", "n=5", "n=7", "besede")), y=casi, width=0.6)) +
  geom_bar(stat="identity", fill="#619CFF") + labs(x="Velikost kosa", y="Pretekel \u010das (s)") + 
  geom_text(aes(label = sprintf("%.2f", casi), hjust = +0.5, vjust = -0.5), size = 8) +
  ylim(0,30) + 
  theme(legend.title = element_text(size=20), legend.text = element_text(size=20), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=26), text = element_text(size=20), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin))

match_time_plot
ggsave("plot_match_time_n.pdf", path=plots_path, plot=match_time_plot, device=cairo_pdf)
#ggsave("plot_match_errors_base.pdf", path=plots_path, plot=plot_match_errors_base)

###measures comparison
data_match_errors_cosine <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_prod_n3.csv"))
data_match_errors_3grams <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_3grams.csv"))
data_match_errors_dist_sim <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_dist_sim.csv"))
data_match_errors_substr <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_substr.csv"))
data_match_errors_jaccard <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_word_jaccard.csv"))
data_match_errors_jaccard_n7 <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_word_jaccard_n7.csv"))

data_measure_time = setNames(data.frame(matrix(ncol = 3, nrow = 5)), c("i", "mera", "MAE"))
data_measure_time$mera = c("kosinusna", "3-grami", "frekv. znakov", "podnizi", "jaccard")
data_measure_time$i = c(1:5)

tmp_MAE = c()
tmp_MAE = c(tmp_MAE, tail(data_match_errors_cosine$MAE, 1))
tmp_MAE = c(tmp_MAE, tail(data_match_errors_3grams$MAE, 1))
tmp_MAE = c(tmp_MAE, tail(data_match_errors_dist_sim$MAE, 1))
tmp_MAE = c(tmp_MAE, tail(data_match_errors_substr$MAE, 1))
tmp_MAE = c(tmp_MAE, tail(data_match_errors_jaccard$MAE, 1))

data_measure_time$MAE = tmp_MAE

ggplot(data_measure_time, aes(x=factor(mera, c("kosinusna", "3-grami", "frekv. znakov", "podnizi", "jaccard")), y=MAE, width=0.6)) +
  geom_bar(stat="identity", fill="#619CFF") + labs(x="Mera", y="Povp. abs. napaka") + 
  geom_text(aes(label = sprintf("%.2f", MAE), hjust = +0.5, vjust = -0.5), size = 8) +
  ylim(0,133)+
  theme(legend.title = element_text(size=20), legend.text = element_text(size=20), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=26), text = element_text(size=20), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin))


ggsave("plot_measure_MAE.pdf", path=plots_path, device=cairo_pdf)

###nounised comparison
data_match_errors_cosine_n3 <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_prod_n3.csv"))
data_match_errors_cosine_n3_nn <- read.csv(file = file.path("not_nounised_db_top600_ings", "match_errors_base_final_prod_n3.csv"))

data_match_errors_cosine_n5 <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_prod_n5.csv"))
data_match_errors_cosine_n5_nn <- read.csv(file = file.path("not_nounised_db_top600_ings", "match_errors_base_final_prod_n5.csv"))

data_match_errors_cosine_n7 <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_prod_n7.csv"))
data_match_errors_cosine_n7_nn <- read.csv(file = file.path("not_nounised_db_top600_ings", "match_errors_base_final_prod_n7.csv"))

data_match_errors_3grams <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_3grams.csv"))
data_match_errors_3grams_nn <- read.csv(file = file.path("not_nounised_db_top600_ings", "match_errors_base_3grams.csv"))

data_match_errors_substr <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_substr.csv"))
data_match_errors_substr_nn <- read.csv(file = file.path("not_nounised_db_top600_ings", "match_errors_base_substr.csv"))

data_match_errors_jaccard <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_word_jaccard.csv"))
data_match_errors_jaccard_nn <- read.csv(file = file.path("not_nounised_db_top600_ings", "match_errors_base_jaccard.csv"))

data_match_errors_dist_sim <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_dist_sim.csv"))
data_match_errors_dist_sim_nn <- read.csv(file = file.path("not_nounised_db_top600_ings", "match_errors_base_dist_sim.csv"))

data_comp_plot = setNames(data.frame(matrix(ncol = 3, nrow = 14)), c("mere", "jeSamostalnik", "MAE"))

data_comp_plot$mere = c(rep("n=3",2), rep("n=5",2), rep("n=7", 2), rep("3-grami", 2), rep("podnizi", 2), rep("jaccard", 2), rep("frek. znak.", 2))
data_comp_plot$jeSamostalnik = rep(c("Samost.", "Cel. imena"),7)

data_comp_plot$MAE[1:2] = c(tail(data_match_errors_cosine_n3$MAE,1),tail(data_match_errors_cosine_n3_nn$MAE,1))
data_comp_plot$MAE[3:4] = c(tail(data_match_errors_cosine_n5$MAE,1),tail(data_match_errors_cosine_n5_nn$MAE,1))
data_comp_plot$MAE[5:6] = c(tail(data_match_errors_cosine_n7$MAE,1),tail(data_match_errors_cosine_n7_nn$MAE,1))
data_comp_plot$MAE[7:8] = c(tail(data_match_errors_3grams$MAE,1),tail(data_match_errors_3grams_nn$MAE,1))
data_comp_plot$MAE[9:10] = c(tail(data_match_errors_substr$MAE,1),tail(data_match_errors_substr_nn$MAE,1))
data_comp_plot$MAE[11:12] = c(tail(data_match_errors_jaccard$MAE,1),tail(data_match_errors_jaccard_nn$MAE,1))
data_comp_plot$MAE[13:14] = c(tail(data_match_errors_dist_sim$MAE,1),tail(data_match_errors_dist_sim_nn$MAE,1))

ggplot(data_comp_plot, aes(fill=jeSamostalnik, x=factor(mere, c("n=3", "n=5", "n=7", "3-grami", "podnizi", "jaccard", "frek. znak.")), y=MAE, width=0.8)) + 
  geom_bar(position="dodge", stat="identity") + labs(x="Mera", y="Povp. abs. napaka") + theme(legend.title=element_blank()) +
  theme(legend.text = element_text(size=18), legend.key.size = unit(1.5, "line"), axis.title = element_text(size=26), text = element_text(size=17), axis.title.x = element_text(margin=x_title_margin), axis.title.y = element_text(margin=y_title_margin))


ggsave("plot_nounise_comp.pdf", path=plots_path, device=cairo_pdf)


###time_info_full_not_full
data_match_errors_all_allings <- read.csv(file = file.path("not_nounised_all", "match_errors_base_final_prod_n3.csv"))
data_match_errors_top600_600ings <- read.csv(file = file.path("nounised_db_top600_ings", "match_errors_base_final_prod_n3.csv"))
data_match_errors_top600_allings <- read.csv(file = file.path("nounised_db_top_600_all_ings", "match_errors_base_final_prod_n3.csv"))

time = c(1:3)
time[1] = data_match_errors_all_allings$elapsed_time[20]
time[2] = data_match_errors_top600_allings$elapsed_time[20]
time[3] = data_match_errors_top600_600ings$elapsed_time[20]

time = time*2000/20


#baseline
random_data <- read.csv(file = 'random_scores.csv')
random_nut <- mean(random_data[,'nut_score'])
random_pref <- mean(random_data[,'pref_score'])
random_homo <- mean(random_data[,'homo_score'])
random_div <- mean(random_data[,'div_score'])
random_count <- sum(random_data[,'div_score'] < 1)


#ggplot(data_match_errors_base, aes(x=i, y=MAE))+geom_line()
#ggsave("plot_random_score.pdf", path=plots_path, plot=plot_match_errors_base)

###########################HOMO JE ZDEJ HETEROGEN SCORE IN MORAS ODSTET OD 1 CIFRO!!!