import scripts.indexMap.helper_functions as hf
import pandas as pd

ingredientSetPath = "../data/ingredients/epi_ingredientSet_nounised.txt"
ingredientListPath = "../data/ingredients/epi_ingredientList_nounised.txt"
n = 500 #how many top ingredients to consider

recipesWithNTopIngredientsPath = "../data/ids_top_"+str(n)+"_ingredients_train.txt"

ingredient_set = set()

count = 0
ids = set() #which recipes contain an ingredient from the set
i = 0

with open(ingredientSetPath, encoding='utf-8') as f:
    for line in f:
        if(n==0):
            break

        arr = line.split(";")
        ingredient_set.add(arr[0])
        n-=1

recipe_ingredient_set = set()
prev_recipe_id = 0
recipe_id = 0
tolerance = 0.88

with open(ingredientListPath, encoding='utf-8') as f:
    for line in f:
        arr = line.split(";")

        if('none' in arr):
            continue

        quantity = float(arr[0])
        ing_name = arr[2]
        recipe_id = int(arr[3])

        if(prev_recipe_id != recipe_id):
            intersected_ings = recipe_ingredient_set.intersection(ingredient_set)

            if((len(intersected_ings) != 0) and (len(intersected_ings) >= len(recipe_ingredient_set)*tolerance)):
                ids.add(prev_recipe_id)
                count += 1

            recipe_ingredient_set = set()
            recipe_ingredient_set.add(ing_name)
            prev_recipe_id = recipe_id
            i += 1
            if (i % 100 == 0):
                print(str(i) + ": current count - " + str(count))
        else:
            recipe_ingredient_set.add(ing_name)

f = open(recipesWithNTopIngredientsPath, "w", encoding='utf-8')

for id in ids:
    f.write(str(id)+"\n")
f.close()