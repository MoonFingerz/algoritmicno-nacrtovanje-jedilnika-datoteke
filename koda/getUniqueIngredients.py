import re
from collections import defaultdict
import scripts.indexMap.helper_functions as hf

ingredients = defaultdict(int)
i = 0

with open("../data/ingredients/epi_ingredientList_nounised.txt", encoding="utf-8") as f:
    for line in f:
        #lineArr = line.split(";")
        #line = lineArr[0]
        arr = line.split(";")
        ing_name = arr[2]

        ingredients[ing_name] += 1
        #quantity, measure, ing_name, recipe_id = hf.smartSplitIngredientTerm(line, True)
        #ingredients.add(l.strip())
        #print("debug")

        if(i%1000 == 0):
            print(i)
        i+=1

ingredients = {k: v for k, v in sorted(ingredients.items(), key=lambda item: item[1], reverse=True)}

ingredientList = ""
for i,count in ingredients.items():
    ingredientList += i + ";" + str(count) + "\n"

with open("../data/ingredients/epi_ingredientSet_nounised.txt", "w", encoding="utf-8") as text_file:
    text_file.write(ingredientList)