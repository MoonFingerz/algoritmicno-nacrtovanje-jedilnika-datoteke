import pandas as pd
import scripts.indexMap.helper_functions as hf
from scripts.indexMap.ingredient import *
from collections import defaultdict
import math

#db_recipes_path = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes.json"
#db_recipes_path = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal.json"
#db_recipes_path = "../data/dbs/epi_recipes_grams_final_normalised100kCal.json"

db_recipes_path = "../data/dbs/epi_recipes_grams_final_normalised100kCal_equiv_classes_sauces_desserts_titles_removed.json"
matrix_path = "../data/dbs/matrix"

recipes = pd.read_json(db_recipes_path)
recipes.index = recipes["index"]

col_list = ['title', 'fat', 'carbohydrate', 'protein', 'sodium', 'sugars', 'calories']

recipes_nut = recipes[col_list]
recipes_nut.to_csv("../data/dbs/orange_nutrients_only.csv")
allIngredients = set()

category_to_export = "all"

for index, row in recipes.iterrows():
    lowercase_categories = list(map(str.lower, row.categories))

    if(category_to_export != "all"):
        if (category_to_export not in lowercase_categories):
            continue

    for ing in row.ingredientsGrams:
        arr = ing.split(";")
        quantity = float(arr[0])
        measure = arr[1]
        ing_name = arr[2]

        allIngredients.add(ing_name)

allIngredients = sorted(list(allIngredients))

#with open('../results/ingredients_sorted_equiv_class_removed_rare_ing.txt', 'w') as f: #OUTPUT LIST OF SORTED INGREDIENTS
#    for item in allIngredients:
#        f.write("%s\n" % item)

matrix = pd.DataFrame()

for ing in allIngredients:
    matrix[ing] = 0.0

count = 0

for index, row in recipes.iterrows():
    lowercase_categories = list(map(str.lower, row.categories))

    if(category_to_export != "all"):
        if (category_to_export not in lowercase_categories):
            continue

    dict_recipe = {}
    matrix = matrix.append(pd.Series(name=index, dtype="float64"))

    for ing in row.ingredientsGrams:
        arr = ing.split(";")
        quantity = float(arr[0])
        measure = arr[1]
        ing_name = arr[2]

        matrix.at[index,ing_name] = quantity
        #dict_recipe[ing_name] = [quantity]

    #recipe_row = pd.DataFrame.from_dict(dict_recipe)
    #matrix = matrix.append(recipe_row)

    if(count%100 == 0):
        print(count)
    count += 1

matrix.fillna(0.0, inplace=True)
matrix.to_json(matrix_path + "_" + category_to_export + "_normalised_100kCal" + ".json")
#matrix.to_csv(matrix_path + "_" + category_to_export + "_equiv_classes" + ".csv")
#matrix.to_csv("../data/dbs/orange_db_equiv_classes_removed_ing_" + category_to_export + ".csv")

print("debug")

