import pandas as pd
import scripts.indexMap.helper_functions as hf
from scripts.indexMap.ingredient import *
from collections import defaultdict
import time

n = 3 #-1 for word match

nounised = "../data/dbs/full_format_recipes_pruned_nounised.json"
#nounised = "../data/dbs/full_format_recipes_pruned.json"
nutrient_info_file = "../data/nutrients/epi_top600.csv"
top_recipe_ids = "../data/ingredients/epi_top_500_recipe_ids_88_tol.txt"
#top_recipe_ids = "../data/ingredients/test_500.txt"
#nutrient_info_file = "../data/nutrients/food_nutrient_info_lowercase.csv"
#top_recipe_ids = "" #WATCH OUT YOU COMMENTED OUT TOP RECIPE IDS, UNCOMMENT THEM!!, YOU ALSO HAVE SMART SPLIT INSTEAD OF REGULAR SPLIT!
nutrient_names = ['fat', 'calories', 'protein']

OUTPUT_RECIPE_MATCHES = False
ING_MATCHES = set()

recipe_ids_set = None

if(top_recipe_ids != ""):
    recipe_ids_set = set()
    with open(top_recipe_ids) as f:
        for id in f:
            recipe_ids_set.add(int(id))

ing_map = IngredientMap(nutrient_info_file)
ing_map.loadIngredients()
n_char_map = hf.createNCharIndex(ing_map, n)

recipes = pd.read_json(nounised)
recipes.index = recipes["index"]

#TODO try out different similarity measures, define new sim measure for finding ingredients (not a set, but something that takes into account all the chars)
totalMAE = 0
nutrient_errors = defaultdict(int)

count = 0
pos = 0
neg = 0

graph_df = pd.DataFrame(columns=['i','PFE', 'NFE', 'TFE', 'PCE', 'NCE', 'TCE', 'PPE', 'NPE', 'TPE', 'totalMAE','MAE','elapsed_time'])
graph_index = 0
#graph_df.loc[0] = [0,1,2,3,4,5,6,7,8]
t = time.time()

for i, row in recipes.iterrows():
    if(recipe_ids_set != None and i not in recipe_ids_set):
        continue

    name = recipes.title[i]
    ingredients = recipes.ingredients[i]

    #ids = [idFromNutrient(n) for n in nutrients]
    recipe_nutrients = recipes.loc[i,nutrient_names]

    nutrients_tally = {nut : 0 for nut in nutrient_names}

    if(OUTPUT_RECIPE_MATCHES):
        print("\n\nRecipe id: #" + str(i))

    for ing in ingredients:
        arr = ing.split(";")
        if('None' in arr):
            continue
        #quantity, measure, ing_name, recipe_id = hf.smartSplitIngredientTerm(ing, False);
        #if(quantity == None or measure == None or ing_name == None):
        #    continue
        quantity = float(arr[0])
        measure = arr[1]
        ing_name = arr[2]
        #quantity, measure, ing_name, recipe_id = hf.smartSplitIngredientTerm(ing, False)
        retr_ing, sim = hf.findInMap(ing_map, n_char_map, n, ing_name)

        #print(ing.name)
        if(retr_ing == None):
            continue
        if (OUTPUT_RECIPE_MATCHES):
            print(ing + " -> " + retr_ing.name)
            ING_MATCHES.add(ing_name + " -> " + retr_ing.name)

        for nut in nutrient_names:
            ing_measure, ing_similarity = hf.findMostSimilarMeasure(retr_ing.nutr_info[idFromNutrient(nut)], measure)
            to_add = (quantity / ing_measure.amount) * ing_measure.nutr_value
            nutrients_tally[nut] += to_add
            #nutrients_tally[i] +=

    normalisedNutrients, servings = hf.estimateNumberOfServings(nutrients_tally, recipe_nutrients, nutrient_names)

    recipes.at[i,"noOfServings"] = servings

    for nut in nutrient_names:
        if(normalisedNutrients[nut] > recipe_nutrients[nut]):
            nutrient_errors["pos_"+nut] += abs(normalisedNutrients[nut] - recipe_nutrients[nut])
            pos += 1
        else:
            nutrient_errors["neg_" + nut] += abs(normalisedNutrients[nut] - recipe_nutrients[nut])
            neg += 1

    MAE = hf.getMAE(normalisedNutrients, recipe_nutrients)
    totalMAE += MAE
    elapsed = time.time() - t
    graph_df.loc[graph_index] = [graph_index, nutrient_errors["pos_fat"]/(count + 1), nutrient_errors["neg_fat"]/(count + 1), (nutrient_errors["pos_fat"]+nutrient_errors["neg_fat"])/(count + 1), nutrient_errors["pos_calories"]/(count + 1), nutrient_errors["neg_calories"]/(count + 1), (nutrient_errors["neg_calories"]+nutrient_errors["pos_calories"])/(count + 1), nutrient_errors["pos_protein"]/(count + 1), nutrient_errors["neg_protein"]/(count + 1), (nutrient_errors["pos_protein"]+nutrient_errors["neg_protein"])/(count + 1), totalMAE, totalMAE/(count+1), elapsed]
    graph_index += 1;
    #print("Total MAE: " + str(totalMAE/(i+1)))

    if(count%100 == 0):
        print("Count:" + str(count) + " Index:" + str(i))
        for nut in nutrient_names:
            print("Positive " + nut + " error: " + str(nutrient_errors["pos_"+nut] / (count+1)) + "\n")
            print("Negative " + nut + " error: " + str(nutrient_errors["neg_"+nut] / (count+1)) + "\n")
            print("Total " + nut + " error: " + str((nutrient_errors["pos_"+nut]+nutrient_errors["neg_"+nut])/ (count+1)) + "\n")

        print("Total MAE: " + str(totalMAE / (count + 1)))
        graph_df.to_csv("../r_visualisation/match_errors_base.csv")
    count += 1

if(OUTPUT_RECIPE_MATCHES):
    with open('../results/full_ing_matches.txt', 'w') as f:
        for item in ING_MATCHES:
            f.write(item+"\n")

#recipes = hf.getSubsetOfDataframe(recipes,list(recipe_ids_set))
#recipes.to_json("../data/dbs/full_format_recipes_pruned_nounised_wNoServings.json")