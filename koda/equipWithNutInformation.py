import pandas as pd
import scripts.indexMap.helper_functions as hf
from scripts.indexMap.ingredient import *
from collections import defaultdict
from pint import UnitRegistry
ureg = UnitRegistry()

nutrient_info_file = "../data/nutrients/epi_top600.csv"
top_recipe_ids = "../data/ingredients/epi_top_500_recipe_ids_88_tol.txt"

nutrient_info_file = "../data/nutrients/epi_top600.csv"
nounised = "../data/dbs/full_format_recipes_pruned_nounised.json"
nutrient_names = ['carbohydrate', 'sugars']

n = 3

ing_map = IngredientMap(nutrient_info_file)
ing_map.loadIngredients()
n_char_map = hf.createNCharIndex(ing_map, n)

recipes = pd.read_json(nounised)
recipes.index = recipes["index"]

ingredientsGrams = []
recipes["ingredientsGrams"] = [[]] * recipes.shape[0]

indices_set = hf.loadIndices(top_recipe_ids)
recipes = recipes.loc[indices_set]

#no_people_db = pd.read_json("../data/dbs/full_format_recipes_pruned_nounised_wNoServings.json")
#recipes.index = recipes["index"]

DEBUG = False
#s = pd.Series([[] for i in range(3)])

#recipes = hf.getSubsetOfDataframe(recipes, hf.getIdsFromFile(top_recipe_ids))

count = 0
indices_inconsistency_sugar = set()

for i, row in recipes.iterrows():
    #if(i not in indices_set):
    #    continue

    name = recipes.title[i]
    ingredients = recipes.ingredients[i]
    nutrients_tally = defaultdict(int)
    gramIngredients = []
    recipes.at[i, "ingredientsGrams"] = []

    totalSugars = 0

    for ing in ingredients:
        arr = ing.split(";")
        if('None' in arr):
            continue
        quantity = float(arr[0])
        measure = arr[1]
        ing_name = arr[2]
        #quantity, measure, ing_name, recipe_id = hf.smartSplitIngredientTerm(ing, False)
        retr_ing, sim = hf.findInMap(ing_map, n_char_map, n, ing_name)

        if(retr_ing == None or sim < 0.1):
            recipes_grams_string = str(0) + ";g;" + ing_name
            recipes.at[i, "ingredientsGrams"].append(recipes_grams_string)
            continue

        grams = 0

        for nut in nutrient_names:
            ing_measure, ing_similarity = hf.findMostSimilarMeasure(retr_ing.nutr_info[idFromNutrient(nut)], measure)

            if(ing_measure == None or ing_similarity < 0.2):
                measure_set = hf.extractAllPermissibleMeasure(retr_ing.nutr_info[idFromNutrient(nut)])
                quantity_pint = None
                unit = None
                m_index = -1

                quantity_cups, unit_cups = hf.convertUnit(quantity, measure, DEBUG)

                if(unit_cups == "cups"):
                    for m in measure_set:
                        quantity_pint, unit = hf.convertUnit(1, m[1], DEBUG)
                        m_index = m[0]

                        if(quantity_pint == None):
                            continue

                        converted_measure = retr_ing.nutr_info[idFromNutrient(nut)][m_index]
                        to_add = converted_measure.nutr_value/(quantity_pint/quantity_cups)*(1/converted_measure.amount)
                        if (to_add < 1000):
                            nutrients_tally[nut] += (to_add)
                        #to_add = converted_measure.nutr_value * quantity_cups

                        if(unit == "cups"):
                            break
            else:
                to_add = (quantity / ing_measure.amount) * ing_measure.nutr_value

                if(to_add < 1000):
                    nutrients_tally[nut] += (to_add)

        quantity_pint, unit = hf.convertUnit(quantity, measure, DEBUG)

        if(unit == "cups"):
            measure = unit

        ing_measure, ing_similarity = hf.findMostSimilarMeasure(retr_ing.nutr_info[idFromNutrient("calories")], measure)

        sizes = ["large", "medium", "small", "jumbo"]

        if(unit == "grams"):
            grams = quantity_pint
        elif(unit == "cups"):
            grams = quantity_pint * ing_measure.grams
        else:
            grams = quantity * ing_measure.grams

        if((measure in sizes) and (ing_measure.measure_name not in sizes) and ing_measure.grams > 5 and ing_similarity < 0.4):
            grams = quantity*50

        #gramIngredients.append(str(quantity*grams)+";g;"+ing_name)

        if("sugar" in ing_name.lower()):
            totalSugars += grams

        recipes_grams_string = str(grams)+";g;"+ing_name
        recipes.at[i, "ingredientsGrams"].append(recipes_grams_string)

        if(DEBUG):
            print(ing + " -> " + recipes_grams_string)

    #recipes.at[i, "ingredientsGrams"] = gramIngredients
    count += 1
    if (count % 100 == 0):
        print(count)

    for nut in nutrient_names:
        recipes.at[i, nut] = nutrients_tally[nut]

    if(totalSugars > recipes.loc[i, "sugars"]):
        indices_inconsistency_sugar.add(i)

#recipes.to_json("../data/dbs/epi_recipes_all_equipped_grams.json")

#recipes.drop(list(indices_inconsistency_sugar), inplace=True)
recipes.to_json("../data/dbs/epi_recipes_grams_final.json")