import pandas as pd
import scripts.indexMap.helper_functions as hf
from collections import defaultdict

recipes_file = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal.json"

#recipes_file = "../data/dbs/full_format_recipes_pruned_nounised.json"
recipes_file = "../data/dbs/epi_recipes_grams_final_normalised100kCal.json"
equiv_file = "../results/ingredients_equiv_classes.txt"

recipes = pd.read_json(recipes_file)
recipes.index = recipes["index"]

equiv_dict = defaultdict(set)

with open(equiv_file) as f:
    for line in f:
        arr = line.split(";")
        key = arr[0]

        ingredients = arr[1].split(",")

        for ing in ingredients:
            equiv_dict[key].add(ing.strip())

for index, row in recipes.iterrows():
    ingredientsGrams = []
    ingredients = []

    for ing in row.ingredientsGrams:
        arr = ing.split(";")

        equiv_class = hf.matchIngToEquivClass(arr[2], equiv_dict)

        ingredientsGrams.append(arr[0]+";"+arr[1]+";"+equiv_class)

    for ing in row.ingredients:
        arr2 = ing.split(";")

        equiv_class_2 = hf.matchIngToEquivClass(arr2[2], equiv_dict)

        ingredients.append(arr[0]+";"+arr[1]+";"+equiv_class_2)

    recipes.at[index, 'ingredientsGrams'] = ingredientsGrams
    recipes.at[index, 'ingredients'] = ingredients

#recipes.to_json("../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes.json")
recipes.to_json("../data/dbs/epi_recipes_grams_final_normalised100kCal_equiv_classes.json")