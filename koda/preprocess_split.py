import scripts.indexMap.helper_functions as hf
import pandas as pd

train_recipes_file = "../data/dbs/full_format_recipes_pruned.json"
train_recipes = pd.read_json(train_recipes_file)
train_recipes.index = train_recipes["index"]


for i, row in train_recipes.iterrows():
    for j in range(0, len(train_recipes.ingredients[i])):
        quantity, measure, ing_name, _ = hf.smartSplitIngredientTerm(train_recipes.ingredients[i][j], False)

        nouns = hf.extractNouns(ing_name)
        train_recipes.at[i, 'ingredients'][j] = str(quantity) + ";" + str(measure) + ";" + nouns

        #print(train_recipes.ingredients[i])

    if (i % 100 == 0):
        print(i)

train_recipes.to_json("../data/dbs/full_format_recipes_pruned_nounised.json")