import pandas as pd

train_recipes_file = "../data/dbs/full_format_recipes"
recipes_file = "../data/dbs/allrecipes-recipes"

DB_TO_PRUNE = "TRAIN" #ops: TRAIN, ALLRECIPES

if(DB_TO_PRUNE == "TRAIN"):
    train_recipes = pd.read_json(train_recipes_file+".json")
    cols = ['ingredients','calories','sodium','protein','fat']

    train_recipes = train_recipes.dropna(how='all')
    train_recipes = train_recipes.dropna(subset=['ingredients','calories','sodium','protein','fat'])

    train_recipes = train_recipes[train_recipes.calories < 5000]
    train_recipes = train_recipes.dropna()
    train_recipes = train_recipes.reset_index(col_fill="recipe_id")

    train_recipes.to_json(train_recipes_file+"_pruned_removed.json")
    #train_recipes.to_csv(train_recipes_file+"_pruned.csv", index=False)
elif(DB_TO_PRUNE == "ALLRECIPES"):
    recipes = pd.read_json(recipes_file+".json", lines=True)
    recipes = recipes.dropna(how='all')
    ix = recipes.author == "The Kitchen at Johnsonville Sausage"
    recipes = recipes[~ix]
    recipes = recipes.reset_index()

    recipes.to_json(recipes_file+"_pruned.json")