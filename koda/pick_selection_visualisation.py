import random
import copy

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
import numpy as np

def count(ls, nu):
    no = 0
    for i in ls:
        if(i == nu):
            no += 1

    return no

def tournamentSelection(N, n):
    avg_N = 3
    winners_dict = {}
    for i in range(0, avg_N):
        winners = []
        for i in range(0, N):
            sampled = random.sample(range(0, N), n)
            winner_index = min(sampled)

            winners.append(winner_index)

        for i in range(0, N):
            if(i in winners_dict.keys()):
                winners_dict[i] += count(winners, i)
            else:
                winners_dict[i] = count(winners, i)

    maks = 0
    for i in range(0,N):
        val = winners_dict[i]/avg_N
        maks = max(val, maks)

        winners_dict[i] = [val]

    return winners_dict, maks

def topNSelection(N, n, fitness):
    winners_dict = {}
    maks = 0
    for i in range(0, N):
        sampled = random.sample(range(0, N), n)
        winner_index = min(sampled)

        pickNumber = int(round(n * fitness[i] / fitness[0]))
        maks = max(maks, pickNumber)
        winners_dict[i] = [pickNumber]

    return winners_dict, maks

fit = [0 for i in range(100)]
fitness_val = 1

for i in range(100):
    fit[i] = fitness_val
    fitness_val -= random.random()*0.05

chosen, maksimum = tournamentSelection(100, 3)
#chosen, maksimum = topNSelection(100, 5, fit)
df = pd.DataFrame.from_dict(chosen)
df = pd.DataFrame(df.values.reshape(10,10))

sb.set(font_scale=2.15)
fig, ax = plt.subplots(figsize=(10, 10))
ax.xaxis.tick_top()

labels = np.zeros(shape=(10,10), dtype=object)
np.set_printoptions(suppress=True)
for i in range(0,10):
    for j in range(0,10):
        labels[i][j] = str(str(10*i+j+1)+".")

grid = sb.heatmap(df, annot = labels, cmap="Blues", vmin=0, vmax=maksimum, linewidth=0.9, cbar=False, cbar_kws={"shrink":.8, "orientation":"horizontal"}, fmt = 's', square=True)

#plt.plot()
plt.tick_params(axis='x', which='both', bottom = False, top=False, labelbottom = False)
plt.axis('off')

plt.show()