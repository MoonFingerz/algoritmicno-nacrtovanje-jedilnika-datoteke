import scripts.indexMap.ingredient as ing
import scripts.indexMap.helper_functions as hf
import textblob as tb

from scripts.indexMap.ingredient import *

def testSearch(ing_map):
    n = 3
    n_char_map = hf.createNCharIndex(ing_map, n)
    ingredient = hf.findInMap(ing_map, n_char_map, n, "milk")
    print("debug: ingredient name = " + str(ingredient[0].name))

def testSplit(ingredient_list_file):
    i = 0
    debug = 100

    with open(ingredient_list_file) as f:
        try:
            for line in f:
                quantity, measure, ing_name, recipe_id = hf.splitIngredientTerm(line)

                blob = tb.TextBlob(ing_name)

                noun_name = hf.extractNouns(blob.tags)
                #print(line.strip() + " -> " + noun_name)

                i +=1
                if(i%100 == 0):
                    print(i)
        except:
            print(line)
            print(i)

def main():
    n = 3 #-1 for word match

    nutrient_info_file = "../data/food_nutrient_info_lowercase.csv"
    ingredient_list_file =  "../data/ingredientList.txt"
    ingredient_list_file = "../data/ingredientList_nouns.txt"

    ing_map = IngredientMap(nutrient_info_file)
    ing_map.loadIngredients()
    n_char_map = hf.createNCharIndex(ing_map, n)

    #testSearch(ing_map)
    #testSplit(ingredient_list_file)
    #implement memoization!!!!!!!!
    #ingredient_map (maps searchs to ingredient) - first try naive approach just the entire string after you strip the measure and the quantity
    #if egg appears twice, just return the object you already mapped first, don't search for it again!
    memo_map = {} #memoized map, maps search string to ingredient object
    i = 0

    with open(ingredient_list_file) as f:
        for line in f:
            #quantity, measure, ing_name, recipe_id = hf.splitIngredientTerm(line) #change later to measure, ingredient etc, maybe try using Jaccard word similarity??
            arr = line.split(";")
            quantity = arr[0]
            measure = arr[1]
            ing_name = arr[2]
            recipe_id = arr[3].strip() #remove "\n"

            if(ing_name in memo_map.keys()):
                continue

            ing, sim = hf.findInMap(ing_map, n_char_map, n, ing_name)
            print(ing_name + "->" +  ing.name)
            i += 1

            memo_map[ing_name] = ing
            if(i%100 == 0):
                print("Mapping:" + i)


    print("debug")

if __name__== "__main__":
    main()