#SAMPLE RANDOM LINES FROM FILE (used for determining match quality of nounised vs. not nounised words)
import random

filename = "../results/full_ing_matches"

with open(filename+".txt") as file:
    lines = []
    for line in file:
        ings = line.split("->")
        lines.append(ings[0].strip() + "\t" + ings[1].strip())

sampled = random.sample(lines, 200)

f = open(filename+"_samples.txt", "w")
for sample in sampled:
    f.write(sample+"\n")
f.close()
