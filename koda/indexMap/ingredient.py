from collections import defaultdict
import locale
locale.setlocale(locale.LC_ALL, 'de_DE')

def idFromNutrient(nutrient):
    nut_map = {"water":255, "calories":208, "protein": 203, "fat":204, "carbohydrate":205, "sugars": 269, "fiber": 291}
    return nut_map[nutrient]

def nutrientFromId(id):
    nut_map = {255: "water", 208: "calories", 203: "protein", 204: "fat", 205: "carbohydrate", 269: "sugars", 291: "fiber"}
    return nut_map[id]

def groupFromId(id):
    group_map = {100:"Dairy and Egg Products",200:"Spices and Herbs",300:"Baby Foods",400:"Fats and Oils",500:"Poultry Products",600:"Soups, Sauces, and Gravies",700:"Sausages and Luncheon Meats",800:"Breakfast Cereals",900:"Fruits and Fruit Juices",1000:"Pork Products",1100:"Vegetables and Vegetable Products",1200:"Nut and Seed Products",1300:"Beef Products",1400:"Beverages",1500:"Finfish and Shellfish Products",1600:"Legumes and Legume Products",1700:"Lamb, Veal, and Game Products",1800:"Baked Products",1900:"Sweets",2000:"Cereal Grains and Pasta",2100:"Fast Foods",2200:"Meals, Entrees, and Side Dishes",2500:"Snacks",3500:"American Indian/Alaska Native Foods",3600:"Restaurant Foods"}
    return group_map[id]

class Measure:
    nutr_id = -1
    nutr_name = ""
    nutr_value_100g = -1
    nutr_value = -1
    amount = -1
    measure_name = ""
    grams = -1

    def __init__(self, nutr_id, nutr_name, nutr_value, amount, measure_name, grams):
        self.nutr_id = nutr_id
        self.nutr_name = nutr_name
        self.nutr_value_100g = nutr_value
        self.nutr_value = (grams/100)*nutr_value
        self.amount = float(amount.replace(",","."))
        self.measure_name = measure_name
        self.grams = grams

class Ingredient:
    id = -1
    group_id = -1
    name = ""
    nutr_info = None

    def __init__(self, id, name, nutr_id, nutr_name, nutr_value, amount, measure_name, grams, group_id):
        self.id = id
        self.group_id = group_id
        self.name = name
        self.nutr_info = defaultdict(list)

        self.nutr_info[nutr_id].append(Measure(nutr_id, nutr_name, nutr_value, amount, measure_name, grams))

    def addMeasure(self, nutr_id, nutr_name, nutr_value, amount, measure_name, grams):
        self.nutr_info[nutr_id].append(Measure(nutr_id, nutr_name, nutr_value, amount, measure_name, grams))

    def findMeasure(self, nutr_id, measure_name):
        maxSim = -1
        name = ""
        maxMeasure = None

        for m in self.nutr_info[nutr_id]:
            jSim = getJaccardSim(m.measure_name, measure_name)
            if(jSim > maxSim):
                maxSim = jSim
                name = m.measure_name
                maxMeasure = m

        return maxMeasure, name, maxSim


class IngredientMap:
    map_id_ing = {}
    filename = ""

    def __init__(self, filename):
        self.filename = filename
        self.map_id_ing = {}

    def loadIngredients(self):
        with open(self.filename) as file:
            curr_row = 0
            for line in file:
                curr_row += 1

                if (curr_row == 1):
                    continue

                cols = line.split(";")
                id = int(cols[0])
                name = cols[2]
                nutr_id = int(cols[3])
                nutr_name = cols[4]
                nutr_value = locale.atof(cols[5])
                amount = cols[6]
                measure_name = cols[7]
                grams = locale.atof(cols[8])
                group_id = int(cols[9])

                if(id in self.map_id_ing.keys()):
                    self.map_id_ing[id].addMeasure(nutr_id, nutr_name, nutr_value, amount, measure_name, grams)
                else:
                    self.map_id_ing[id] = Ingredient(id, name, nutr_id, nutr_name, nutr_value, amount, measure_name, grams, group_id)

#ing_map = IngredientMap("../data/food_nutrient_info.csv")
#ing_map.loadIngredients()

#print(ing_map.map_id_ing[1001].findMeasure(208, "cups"))