import pandas as pd
import scripts.indexMap.helper_functions as hf
from scripts.Genetic.GeneticAlgorithm import *
from scripts.indexMap.ingredient import *
from collections import defaultdict
import copy

import re

import random

from scipy.sparse import lil_matrix
from scipy.sparse import csr_matrix
from sklearn.decomposition import NMF
import numpy as np
from numpy.random import rand

class Recommender:
    def __init__(self, type, nutr_limit_path, db_path, nutrient_info_file, preference_list_path):
        self.ing_map = IngredientMap(nutrient_info_file)
        self.ing_map.loadIngredients()
        self.n = 3
        self.n_char_map = hf.createNCharIndex(self.ing_map, self.n)
        self.nutrient_list = ['fat', 'carbohydrate', 'protein', 'sodium', 'sugars', 'calories']

        self.type = type
        self.limits = {}
        self.db = pd.read_json(db_path)
        self.db.index = self.db["index"]
        self.pref_list = {}
        self.days = 7
        self.meals = 3
        self.weekly_plan = [[None for _ in range(self.meals)] for _ in range(self.days)]

        with open(nutr_limit_path) as f:
            for line in f:
                arr = line.split(":")
                self.limits[arr[0]] = float(arr[1])

        with open(preference_list_path) as f:
            for line in f:
                quantity, measure, ingr_name, _ = hf.smartSplitIngredientTerm(line, False)
                gram_measures = ["g", "gram", "grams"]
                if(measure not in gram_measures):
                    grams = hf.findGrams(self.ing_map, self.n_char_map, self.n, quantity, measure, ingr_name)
                else:
                    grams = quantity
                self.pref_list[ingr_name] = float(grams)

    def getTotalNutrients(self, d, m):
        nutDict = defaultdict(float)
        if(d!=-1 and m!=-1):
            for d in range(self.days):
                for m in range(self.meals):
                    for nut in self.nutrient_list:
                        nutDict[nut] += self.weekly_plan[d][m][nut]
        else:
            for d in range(self.days):
                for m in range(self.meals):
                    for nut in self.nutrient_list:
                        nutDict[nut] += self.weekly_plan[d][m][nut]*self.mealFactor(m)

        return nutDict

    def nutrient_score(self, is_genetic=False):
        if (self.weekly_plan[0][0].empty):
            return 0, 0, 0

        totals_nutrients = defaultdict(float)

        for d in range(self.days):
            for m in range(self.meals):
                for nut in self.nutrient_list:
                    if(is_genetic):
                        totals_nutrients[nut] += self.weekly_plan[d][m][nut]
                    else:
                        totals_nutrients[nut] += self.weekly_plan[d][m][nut] * self.mealFactor(m)

        nut_score = 0
        for nut in self.nutrient_list:
            weekly_total = (7 * self.limits[nut])

            if (totals_nutrients[nut] <= 2 * weekly_total):
                nut_score += 1 - abs(weekly_total - totals_nutrients[nut]) / weekly_total

        nut_score /= len(self.nutrient_list)

        return nut_score

    def preference_score(self):
        pref_score = 0
        pref_list_copy = self.pref_list.copy()

        for d in range(self.days):
            for m in range(self.meals):
                for ing_rec in self.weekly_plan[d][m].ingredientsGrams:
                    arr = ing_rec.split(";")
                    for ing_pref in self.pref_list:
                        if (hf.getFreqOfMatchingNGrams(arr[2], ing_pref, 3) > 0.5):
                            pref_list_copy[ing_pref] -= float(arr[0])

        for ing, val in pref_list_copy.items():
            if (val <= 0):
                pref_score += 1;

        pref_score /= len(pref_list_copy)

        return pref_score

    def homogeneity_score(self):
        score_homo = 0
        count = 0
        for dm1 in range(self.days * self.meals):
            set_ing1 = set()
            for ing in self.weekly_plan[dm1 // self.meals][dm1 % self.meals].ingredientsGrams:
                arr = ing.split(";")
                set_ing1.add(arr[2])

            for dm2 in range(dm1 + 1, self.days * self.meals):
                set_ing2 = set()
                for ing in self.weekly_plan[dm2 // self.meals][dm2 % self.meals].ingredientsGrams:
                    arr = ing.split(";")
                    set_ing2.add(arr[2])

                total = (len(set_ing1) + len(set_ing2)) / 2  # max possible pairs
                matches = {}
                for ing1 in set_ing1:
                    for ing2 in set_ing2:
                        if (ing1 not in matches.keys() and hf.getFreqOfMatchingNGrams(ing1, ing2, 3) > 0.5):
                            matches[ing1] = ing2
                            set_ing2.remove(ing2)
                            break

                score_homo += len(matches) / total
                count += 1

        score_homo = score_homo / count

        return 1-score_homo

    def diversity_score(self):
        score_diversity = 0
        title_set = defaultdict(set)
        count = 0

        for d in range(self.days):
            for m in range(self.meals):
                title_set[d].add(self.weekly_plan[d][m].title)

        for d in range(self.days - 1):
            intersectionCount = 0
            recipe_matches = {}
            for m1 in range(self.meals):
                for compared_title in title_set[d + 1]:
                    if (self.weekly_plan[d][m1].title not in recipe_matches.keys() and hf.getFreqOfMatchingNGrams(self.weekly_plan[d][m1].title, compared_title, 3) > 0.7):
                        recipe_matches[self.weekly_plan[d][m1].title] = compared_title
                        title_set[d + 1].remove(compared_title)
                        intersectionCount += 1
                        break

            score_diversity += 1 - intersectionCount / self.meals
            count += 1

        score_diversity = score_diversity / count

        return score_diversity

    def score(self, is_genetic):
        ####ADD PARAMETER TO MULT WITH MEAL FACTOR OR NOT!
        return self.nutrient_score(is_genetic), self.preference_score(), self.homogeneity_score(), self.diversity_score()

    def score_genetic(self):
        ###NO MEAL FACTOR IMPLIED HERE
        pass

    def mealFactor(self, meal):
        calorieDist = {0: 5, 1: 10, 2: 5} #multipliers of 100kCal
        return calorieDist[meal]

    def nutFractionFactor(self, meal):
        fracDistribution = {0: 1/4, 1: 1/2, 2: 1/4}
        return fracDistribution[meal]

    def outputRecipes(self, recipes, meal):
        for index, row in recipes.iterrows():
            print(row.title, end="")

            mult_nutrients = hf.multiplyNutrients(row, self.mealFactor(meal), self.nutrient_list)
            for nut in self.nutrient_list:
                print(nut+": " + str(mult_nutrients[nut]), end="; ")

            print()

    def customOutputRecipes(self, recipes, indices, print_pref_list = True):
        prevDay = 0
        dayTotals = defaultdict(float)

        for day, meal in indices:
            if(day != prevDay):
                for nut in self.nutrient_list:
                    print("               ###-----" + nut + ":" + str(dayTotals[nut]), end="; ")
                print("\n")
                dayTotals = defaultdict(float)

            print(str(recipes[day][meal]["index"]) + ":" + recipes[day][meal].title + "### " + str(day)+":"+str(meal)+"-----", end="")
            for nut in self.nutrient_list:
                print(nut+": " + str(recipes[day][meal][nut]), end="; ")
                dayTotals[nut] += recipes[day][meal][nut]

            print()
            prevDay = day

        pref_list_tally = copy.deepcopy(self.pref_list)
        totals_ingredients = defaultdict(float)

        if(print_pref_list == True):
            ing_string = defaultdict(str)

            for day in range(self.days):
                for meal in range(self.meals):
                    ingredients = recipes[day][meal].ingredientsGrams
                    for ing in ingredients:
                        arr = ing.split(";")
                        quant = float(arr[0])
                        ing_name = arr[2]

                        for ing_pref, quant_pref in self.pref_list.items():
                            sim = hf.getFreqOfMatchingNGrams(ing_pref, ing_name, 3)
                            #sim, substr = hf.getSubstringSim(ing_pref, ing_name)

                            if(sim > 0.7):
                                pref_list_tally[ing_pref] -= quant
                                totals_ingredients[ing_pref] += quant

                                ing_string[ing_pref] += str(recipes[day][meal]["index"]) + ":" + ing_name + " " + '{0:.2f}'.format(quant) + "; "
                                break

            for ing_pref_list, quant in self.pref_list.items():
                print(ing_pref_list + " : " + str(totals_ingredients[ing_pref_list]) + " --- " + ing_string[ing_pref_list])

    def outputRecipe(self, row, meal):
        print(row["title"] + ":" + str(row["index"]), end=" ###### ")

        mult_nutrients = hf.multiplyNutrients(row, self.mealFactor(meal), self.nutrient_list)
        for nut in self.nutrient_list:
            print(nut + ": " + str(mult_nutrients[nut]), end="; ")

        print()

    def outputWeeklyMealPlan(self):
        for day in range(0, len(self.weekly_plan)):
            limits = self.limits.copy()

            nutrientSum = defaultdict(float)
            for meal in range(0, len(self.weekly_plan[day])):
                mulNutrients = hf.multiplyNutrients(self.weekly_plan[day][meal], self.mealFactor(meal), self.nutrient_list)

                self.outputRecipe(self.weekly_plan[day][meal], meal)

                for nut, v in mulNutrients.items():
                    nutrientSum[nut] = nutrientSum[nut] + v

            print("Day " + str(day) + " total: ", end="")

            for nut in self.nutrient_list:
                print(nut + ":" + str(nutrientSum[nut]), end=", ")

            print()
            print("Recommended totals: ", end="")

            for nut in self.nutrient_list:
                print(nut + ":" + str(self.limits[nut]), end=", ")
            print("\n")

    def replaceIngredient(self, day, meal, excluded_ing_names, n):
        banned_categories = set(
            ["cupcake", "cookie", "chocolate", "ice cream", "sauce", "dessert", "frozen dessert", "cake", "vinaigrette",
             "drink", "drinks", "fruit juice", "alcoholic", "beer"])
        all_categories = set()

        recipe = self.weekly_plan[day][meal]

        ingredients = recipe.ingredientsGrams
        comp_recipe = defaultdict(float)
        excluded_ing_name_set = set()
        base_categories = set(recipe.categories)

        for excluded_ing in excluded_ing_names:
            excluded_ing_name_set.add(excluded_ing)

        candidates = []
        quant_total = 0

        for ing in ingredients:
            arr = ing.split(";")
            quant_total += float(arr[0])

        for ing in ingredients:
            arr = ing.split(";")
            quant = float(arr[0])
            ing_name = arr[2]

            is_excluded_ing = False

            for excluded_ing in excluded_ing_names:
                sim, substr = hf.getSubstringSim(excluded_ing, ing_name)
                sim = hf.getFreqOfMatchingNGrams(substr, excluded_ing, 3)

                if(sim > 0.8):
                    excluded_ing_name_set.add(ing_name)
                    is_excluded_ing = True

            if(is_excluded_ing == False and quant > 0.05*quant_total):
                comp_recipe[ing_name] = quant

        for i, row in self.db.iterrows():
            if(len(set([c.lower() for c in row.categories]).intersection(banned_categories)) > 0 or len(row.ingredientsGrams) < 3):
                continue

            continueOuterLoop = False

            db_ingredients = row.ingredientsGrams
            db_ing_dict = defaultdict(float)

            for ing in db_ingredients:
                arr = ing.split(";")

                for excluded_str in excluded_ing_name_set:
                    sim, substr = hf.getSubstringSim(arr[2], excluded_str)

                    if(sim > 0.5):
                        continueOuterLoop = True
                        break

                db_ing_dict[arr[2]] = float(arr[0])

            category_bonus = 0

            if(len(set(row.categories).intersection(base_categories)) > 0):
                category_bonus = 0.5

            if(continueOuterLoop):
                continue

            #if(i == 12736):
            #    sim = hf.getCosineSimilarity(db_ing_dict, comp_recipe)

            all_ings_comp = " ".join(comp_recipe.keys())
            db_ings = " ".join(db_ing_dict.keys())

            #sim1 = hf.getCosineSimilarity(db_ing_dict, comp_recipe)
            sim1 = hf.getFreqOfMatchingNGrams(row.title, recipe.title, 3)
            sim2 = hf.getDistSim(db_ing_dict, comp_recipe)
            sim3 = hf.getFreqOfMatchingNGrams(all_ings_comp, db_ings, 3)
            candidates.append((i, sim1 + 2*sim2 + sim3 + category_bonus))

        candidates.sort(key=lambda x: x[1], reverse=True)

        i = -1
        prevTitle = recipe.title.strip()

        while i < n:
            i += 1
            candidate_recipe = self.db.loc[candidates[i][0]]
            title = candidate_recipe.title.strip()

            if(prevTitle == title):
                continue

            print(str(candidates[i][1]), end=": ")
            self.outputRecipe(candidate_recipe, meal)
            ingredientsGrams = hf.multiplyIngRowGrams(candidate_recipe, self.mealFactor(meal))
            print(str(ingredientsGrams) + "\n")

            #print(title, end=" ")
            #print(self.db.loc[candidates[i][0]])

            prevTitle = title

class Random(Recommender):
    def __init__(self, type, nutr_limit_path, db_path, nutrient_info_file, preference_list_path):
        super().__init__(type, nutr_limit_path, db_path, nutrient_info_file, preference_list_path)

    def recommend(self):
        recipes = self.db.sample(self.days*self.meals)

        for i in range(self.days*self.meals):
            self.weekly_plan[i//self.meals][i%self.meals] = recipes.iloc[i]

        self.outputWeeklyMealPlan()

    def outputScores(self, n):
        plot_df = pd.DataFrame(columns=['i', 'nut_score', 'pref_score', 'homo_score', 'div_score', 'fat', 'carbohydrate', 'protein', 'sodium', 'sugars', 'calories'])

        for i in range(n):
            self.recommend()
            nut_score, pref_score, homo_score, div_score = self.score(False)
            total_nuts = self.getTotalNutrients(-1, -1)
            plot_df.loc[i] = [i, nut_score, pref_score, homo_score, div_score, total_nuts['fat'], total_nuts['carbohydrate'], total_nuts['protein'], total_nuts['sodium'], total_nuts['sugars'], total_nuts['calories']]

        plot_df.to_csv("../r_visualisation/random_scores.csv", index=False)

class Heuristic(Recommender):
    def __init__(self, type, nutr_limit_path, db_path, nutrient_info_file, preference_list_path):
        super().__init__(type, nutr_limit_path, db_path, nutrient_info_file, preference_list_path)

    def outputScores(self, n, mult_pref, mult_nut, mult_fat, isRelative):
        plot_df = pd.DataFrame(columns=['i', 'nut_score', 'pref_score', 'homo_score', 'div_score', 'mult_pref', 'mult_nut', 'mult_fat', 'fat', 'carbohydrate', 'protein', 'sodium', 'sugars', 'calories','isRelative'])

        for i in range(n):
            self.recommend(mult_pref, mult_nut, mult_fat, isRelative)
            nut_score, pref_score, homo_score, div_score = self.score(False)
            total_nuts = self.getTotalNutrients(-1, -1)
            plot_df.loc[i] = [i, nut_score, pref_score, homo_score, div_score, mult_pref, mult_nut, mult_fat, total_nuts['fat'], total_nuts['carbohydrate'], total_nuts['protein'], total_nuts['sodium'], total_nuts['sugars'], total_nuts['calories'], str(isRelative)]

        plot_df.to_csv("../r_visualisation/heuristic_scores_"+str(mult_pref)+ "_" + str(mult_nut)+ "_" + str(mult_fat)+ "_" + str(isRelative) + ".csv", index=False)

    def preferenceWeights(self, mult_pref): #if ingredient is in the preference list, boosts probability of a recipe by some factor
        for index, row in self.db.iterrows():
            for ing in row.ingredients: #TODO maybe this should be ingredientsGrams
                quantity, measure, ing_name = ing.split(";")
                if (quantity == 'None' or measure == 'None'):
                    continue

                for pref_ing, grams in self.pref_list.items():
                    if(grams > 0 and hf.getFreqOfMatchingNGrams(ing_name, pref_ing, 3) > 0.5):
                        self.db.at[index, 'weights'] = self.db['weights'][index]*mult_pref #change this prob weight

    def nutrientWeights(self, meal, mult_nut, mult_fat, isRelative):
        fractioned_limits = {nut: limit*self.nutFractionFactor(meal) for nut, limit in self.limits.items()}
        min_err = 9999999999

        for index, row in self.db.iterrows():
            nut_dict = defaultdict(float)
            for nut in self.nutrient_list:
                nut_dict[nut] = row[nut]*self.mealFactor(meal)

            #sim = hf.getCosineSimilarity(fractioned_limits, nut_dict)
            if(isRelative):
                err = hf.getRMAE(fractioned_limits, nut_dict)
                err = err ** 2
            else:
                err = hf.getMAE(fractioned_limits, nut_dict)

            if(err < min_err):
                min_err = err

            if(err < 50):
                if(nut_dict["fat"] > 30 and mult_fat != -1):
                    self.db.at[index, 'weights'] = self.db['weights'][index] * 1/(err+1e-5) * mult_fat
                else:
                    if(mult_nut == -1):
                        self.db.at[index, 'weights'] = self.db['weights'][index] * 1/(err+1e-5)
                    else:
                        self.db.at[index, 'weights'] = self.db['weights'][index] * mult_nut
            else:
                self.db.at[index, 'weights'] = 0

    def validWeights(self, day):
        for recipe in self.weekly_plan[day]:
            for index, row in self.db.iterrows():
                if(recipe is not None and recipe.title == row.title):
                    self.db.at[index, 'weights'] = 0.0

    def recommend(self, mult_pref, mult_nut, mult_fat, isRelative):
        limits = self.limits.copy()
        pref_list = self.pref_list.copy()

        for day in range(0, len(self.weekly_plan)):
            limits = self.limits.copy()

            for meal in range(0, len(self.weekly_plan[day])):
                self.db['weights'] = 1.0  # initialise weights
                self.preferenceWeights(mult_pref)
                self.nutrientWeights(meal, mult_nut, mult_fat, isRelative)
                self.validWeights(day)

                # SELECT_RECIPE
                selected_recipes = self.db.sample(n=1, weights='weights').iloc[0]
                self.weekly_plan[day][meal] = selected_recipes
                #self.outputRecipes(selected_recipes, meal)

                # SUBTRACT FROM PREFERENCE INGREDIENTS
                for ing in selected_recipes.ingredientsGrams: #TODO maybe this should be ingredientsGrams
                    quantity, measure, ing_name = ing.split(";")
                    if (quantity == 'None' or measure == 'None'):
                        continue

                    grams = hf.findGrams(self.ing_map, self.n_char_map, self.n, float(quantity), measure, ing_name)

                    for pref_ing in self.pref_list:
                        if(hf.getFreqOfMatchingNGrams(ing_name, pref_ing, 3) > 0.5):
                            pref_list[pref_ing] -= grams

        self.outputWeeklyMealPlan()


class NNMF(Recommender):
    def __init__(self, type, nutr_limit_path, db_path, nutrient_info_file, preference_list_path, matrix_path, results_path, idToIng_path, concurrent_path):
        super().__init__(type, nutr_limit_path, db_path, nutrient_info_file, preference_list_path)
        self.matrix = pd.read_json(matrix_path+".json")
        self.indices_path = results_path + "_indices.txt"
        self.values_path = results_path + "_values.txt"
        self.matrix_path = matrix_path
        self.idToIng_path = idToIng_path
        self.concurrent_path = concurrent_path

    def fillMatrixWithConcurrent(self):
        #fills from the preference list, concurrent are NaN, the rest are 0
        ing_concurrent_map = hf.loadConcurrent(self.concurrent_path)

        for i in range(self.days * self.meals):
            new_recipe_index = str(i // self.meals) + ":" + str(i % self.meals)
            self.matrix = self.matrix.append(pd.Series(name=new_recipe_index, dtype="float64"))

            for ing_name, quant in self.pref_list.items():
                self.matrix.at[new_recipe_index, ing_name] = quant

        intersected_ing = set()
        self.matrix.fillna(0.0, inplace=True)

        for ing, q in self.pref_list.items():
            if(len(intersected_ing) == 0):
                intersected_ing = ing_concurrent_map[ing]
            else:
                intersected_ing = intersected_ing.intersection(ing_concurrent_map[ing])

        for i in range(self.days * self.meals):
            new_recipe_index = str(i // self.meals) + ":" + str(i % self.meals)
            random_ings = random.sample(intersected_ing, 20)  # select 20 random ingredients

            for ing in random_ings:
                self.matrix.at[new_recipe_index, ing] = np.nan

    def fillMatrixDefault(self):
        #fills only the preference list, rest are NaN
        for i in range(self.days * self.meals):
            new_recipe_index = str(i // self.meals) + ":" + str(i % self.meals)
            self.matrix = self.matrix.append(pd.Series(name=new_recipe_index, dtype="float64"))

            for ing_name, quant in self.pref_list.items():
                self.matrix.at[new_recipe_index, ing_name] = quant

    def exportPreferenceList(self, opt = "default"):
        if(opt == "default"):
            self.fillMatrixDefault()
        elif(opt == "concurrent"):
            self.fillMatrixWithConcurrent()
        else:
            raise Exception("Not a valid option")

        self.matrix.to_csv(self.matrix_path + ".csv", index=False, header=False)

    def getIngredientsFromResults(self, path):
        id_to_ing = hf.loadIdToIngPairs(path)
        weekly_recs = [[None for _ in range(self.meals)] for _ in range(self.days)]

        no_recipe = 0

        with open(self.indices_path) as fi, open(self.values_path) as fv:
            for l1, l2 in zip(fi, fv):
                ing_to_quantity = {}
                l1 = re.sub(', $', '', l1)
                l2 = re.sub(', $', '', l2)
                indices = [int(x.strip()) for x in l1.split(",")]
                values = [float(x.strip()) for x in l2.split(',')]

                for i,v in zip(indices,values):
                    ing_to_quantity[id_to_ing[i-1]] = v

                weekly_recs[no_recipe//self.meals][no_recipe%self.meals] = ing_to_quantity
                no_recipe += 1

        return weekly_recs #returns ing_name:quantity pairs

    def findMostSimilarRecipe(self, ingredients):
        mostSimilar = None
        max_sim = -1

        for index, row in self.db.iterrows():
            sim = hf.calculateSimilarityRowIngredients(row.ingredientsGrams, ingredients)

            if(sim > max_sim):
                max_sim = sim
                mostSimilar = row

        return mostSimilar

    def recommend(self, post_filtering = False):
        res = self.getIngredientsFromResults(self.idToIng_path)

        for day in range(self.days):
            for meal in range(self.meals):
                sampled = hf.randomlySelectFromDict(res[day][meal], lambda x: x > 5 and x < 2000, random.randint(4,8), post_filtering, self.concurrent_path)
                self.weekly_plan[day][meal] = self.findMostSimilarRecipe(sampled)

        self.outputWeeklyMealPlan()
        print("debug")

    def outputScore(self, tag, k_size = 7, restart_file=False, filtering=False):
        #file_path = "../r_visualisation/matrix_scores.csv"
        #file_path = "../r_visualisation/matrix_scores"+"_"+tag+ "_filtering_"+ str(filtering) +".csv"
        file_path = "../r_visualisation/matrix_scores"+"_"+tag+ "_filtering_"+ str(filtering) +"_k_"+str(k_size)+".csv"

        plot_df = pd.DataFrame(columns=['index', 'tag', 'k', 'isFiltered', 'nut_score', 'pref_score', 'hetero_score', 'div_score', 'fat', 'carbohydrate', 'protein', 'sodium', 'sugars', 'calories'])
        i = 0

        if (restart_file == False):
            plot_df = pd.read_csv(file_path)
            i = len(plot_df)

        self.recommend(post_filtering=False)
        nut_score, pref_score, homo_score, div_score = self.score(False)
        total_nuts = self.getTotalNutrients(-1, -1)
        plot_df.loc[i] = [i, tag, str(filtering), k_size, nut_score, pref_score, homo_score, div_score, total_nuts['fat'], total_nuts['carbohydrate'], total_nuts['protein'], total_nuts['sodium'], total_nuts['sugars'], total_nuts['calories']]

        plot_df.to_csv(file_path, index=False)

        #restart_file is a parameter to see if it adds to file or starts filling again from scratch
        #add parameters to other csv exports?? (down the line)
        #make it so that it adds to the csv with every run?

class Genetic(Recommender):
    def __init__(self, type, nutr_limit_path, db_path, nutrient_info_file, preference_list_path, N, n, mutate_chance, normalised, mult_factor, selection_opt):
        super().__init__(type, nutr_limit_path, db_path, nutrient_info_file, preference_list_path)

        self.N = N  # number of generations
        self.n = n  # number of units per generation
        self.generation_list = self.weekly_plan = [[None for _ in range(self.n)] for _ in range(self.N)] #[i][j]; i = generation number, j = sample in generation

        self.mutate_chance = mutate_chance

        self.max_fitness = 0
        self.max_fitness_generation = []
        self.normalised = normalised
        self.mult_factor = mult_factor
        self.selection_opt = selection_opt

    def topNSelection(self, n, curr_gen_i):
        count = 0

        for gen in self.generation_list[curr_gen_i]:
            pickNumber = max(int(round(n * gen.fitness / self.generation_list[curr_gen_i][0].fitness)),1)

            for i in range(pickNumber):
                self.generation_list[curr_gen_i + 1][count] = copy.deepcopy(gen)
                count += 1

                if (count > self.n - 1):
                    break

            if (count > self.n - 1):
                break

    def tournamentSelection(self, n, curr_gen_i, start_index, end_index):
        for i in range(start_index, end_index):
            sampled = random.sample(range(0, self.n), n)
            winner_index = min(sampled)

            winner = self.generation_list[curr_gen_i][winner_index]
            self.generation_list[curr_gen_i+1][i] = copy.deepcopy(winner)

    def crossoverTournament(self, n, curr_gen_i, proportions):
        #(0.1, 0.5, 0.4) ~ 10% best, 50% crossover, 40% tournament
        parents = []
        i = 0

        for i in range(0, int(proportions[0]*self.n)):
            self.generation_list[curr_gen_i+1][i] = copy.deepcopy(self.generation_list[curr_gen_i][i])

        for j in range(i+1, int(proportions[1]*self.n)+i+1):
            sampled = sorted(random.sample(range(0, self.n), n))
            first_parent = self.generation_list[curr_gen_i][sampled[0]]
            second_parent = self.generation_list[curr_gen_i][sampled[1]]

            child = copy.deepcopy(first_parent)
            no_samples = random.randint(5, int(self.days*self.meals/2))
            sampled = random.sample(range(0, self.days*self.meals), no_samples)

            for s in sampled:
                day = s//self.meals
                meal = s%self.meals

                child.recipes[day][meal] = copy.deepcopy(second_parent.recipes[day][meal])

            r = random.random()

            self.generation_list[curr_gen_i + 1][j] = child
            #if(i+1 < self.n):
            #    self.generation_list[curr_gen_i + 1][i+1] = first_parent
            #else:
            #    break

        #for k in range(j, int(proportions[2])+j):
        self.tournamentSelection(5, curr_gen_i, j+1, self.n)


    def generationStep(self, curr_gen_i):
        #mutate, drop a certain number (the ones with lowest fitness, at random), copy into new generation, increase generation_list by one
        self.generation_list[curr_gen_i].sort(reverse = True, key=lambda x: x.fitness)

        if(self.selection_opt == "tournament"):
            self.tournamentSelection(5, curr_gen_i, 0, self.n)
        elif(self.selection_opt == "topN"):
            self.topNSelection(5, curr_gen_i)
        elif(self.selection_opt == "crossoverTournament"):
            self.crossoverTournament(5, curr_gen_i, (0.1, 0.4, 0.5))
        else:
            raise("Not a valid option!")

        print(self.generation_list[curr_gen_i][0].fitness)
        #for gen in self.generation_list[curr_gen_i]:
        #    print(gen.fitness)

        for gen in self.generation_list[curr_gen_i+1]:
            gen.mutate(self.db, self.mutate_chance)


    def normaliseFitness(self, curr_gen_i):
        sum = 0
        for gen_unit in self.generation_list[curr_gen_i]:
            sum += gen_unit.fitness

        for gen_unit in self.generation_list[curr_gen_i]:
            gen_unit.fitness = gen_unit.fitness/sum

    #tag=which tag to append to output file; out_it=multiple of which iteration to output (50 means every 50th iteration); output=True to output, False to not
    def recommend(self, tag, out_it=50, output_to_file = False):
        plot_df = pd.DataFrame(columns=['index', 'tag', 'nut_factor', 'list_factor', 'hete_factor', 'fitness', 'nut_score', 'pref_score', 'homo_score', 'div_score', 'fat', 'carbohydrate', 'protein', 'sodium', 'sugars', 'calories'])
        #create GenerationUnit, populate it

        for i in range(self.N-1):
            for j in range(self.n):
                if(i == 0):
                    self.generation_list[i][j] = GenerationUnit(self.db, self.days, self.meals, self.nutrient_list)

                self.generation_list[i][j].assessFitness(self.pref_list, self.limits, self.normalised, self.mult_factor)

            if (output_to_file):
                self.weekly_plan = max(self.generation_list[i], key=lambda x: x.fitness).recipes
                nut_score, pref_score, homo_score, div_score = self.score(True)
                total_nuts = self.getTotalNutrients(1, 1)
                plot_df.loc[i] = [i, tag, self.mult_factor[0], self.mult_factor[1], self.mult_factor[2], max(self.generation_list[i], key=lambda x: x.fitness).fitness, nut_score, pref_score, homo_score, div_score, total_nuts['fat'], total_nuts['carbohydrate'], total_nuts['protein'], total_nuts['sodium'], total_nuts['sugars'], total_nuts['calories']]

            #self.normaliseFitness(i)
            self.generationStep(i)

            print("Generation " + str(i) + "/" + str(self.N-1))

            if(i%out_it == 0):
                self.customOutputRecipes(self.generation_list[i][0].recipes, [(0,0),(0,1),(0,2),(1,0),(1,1),(1,2),(2,0),(2,1),(2,2),(3,0),(3,1),(3,2),(4,0),(4,1),(4,2),(5,0),(5,1),(5,2),(6,0),(6,1),(6,2)], print_pref_list = True)
                print("Fitness: " + str(self.generation_list[i][0].fitness_scores))

                self.generation_list[i][0].assessFitness(self.pref_list, self.limits, self.normalised, self.mult_factor)
                print()

                if(output_to_file):
                    plot_df.to_csv("../r_visualisation/genetic_scores_" + str(tag) + "_" + str(self.mult_factor[0]) + "_" + str(self.mult_factor[1]) + "_" + str(self.mult_factor[2]) + "_" + self.selection_opt + ".csv", index=False)

        print("debug")

            #self.generationStep()
                #assess each GenerationUnit
                #generationStep, drop the worst, mutate next


