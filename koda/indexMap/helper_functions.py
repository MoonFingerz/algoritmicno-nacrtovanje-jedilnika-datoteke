from collections import defaultdict
import strsimpy as strcmp #don't forget to give credit in the masters for library
import string
import parser
import re
import textblob as tb
import math
import difflib as dl
from collections import Counter
import math
import random
import string
from scripts.indexMap.ingredient import *
from scipy.spatial.distance import cosine
import pandas as pd
import numpy as np

from pint import UnitRegistry
ureg = UnitRegistry()
#from bert_score import score
#from bert_score import plot_example
#import gensim.downloader as api

RE_D = re.compile('\d')
legalChars = set("0123456789" + "(" + ")" + "/" + "*" + " " + "+")

def strToNgrams(string, n):
    ngram = defaultdict(int)

    for i in range(0, len(string)-n+1):
        seq = string[i:i+n]
        ngram[seq] += 1

    return ngram

def convertUnit(quantity, measure, DEBUG):
    res = None
    unit = None

    try:
        res = quantity*ureg.parse_expression(measure)
        res = quantity*ureg.parse_expression(measure)
        res = res.to(ureg.grams)
        unit = "grams"
        if(DEBUG):
            print("Converted " + str(quantity) + " " + measure + " to " + str(res.m) + " grams")
    except Exception as e:
        if(DEBUG):
            print(str(e))

    if(unit == None):
        try:
            res = quantity*ureg.parse_expression(measure)
            res = res.to(ureg.cups)
            unit = "cups"
            if(DEBUG):
                print("Converted " + str(quantity) + " " + measure + " to " + str(res.m) + " cups")
        except Exception as e:
            if(DEBUG):
                print(str(e))

    if(unit != None):
        return res.m, unit
    else:
        return None, None

def getFreqOfMatchingNGrams(str1, str2, n):
    if(str1 == None or str2 == None):
        return 0

    sen1 = [word.strip(string.punctuation) for word in str1.split()]
    sen2 = [word.strip(string.punctuation) for word in str2.split()]
    set1 = set()
    set2 = set()

    if(len(str1) < n):
        set1.add(str1)

    if(len(str2) < n):
        set2.add(str2)

    #set1.add(str1)
    #set2.add(str2)

    for w in sen1:
        ngrams = set([w[i:i+n] for i in range(0, len(w)-n+1)])
        set1 = set1.union(ngrams)

    for w in sen2:
        ngrams = set([w[i:i + n] for i in range(0, len(w) - n + 1)])
        set2 = set2.union(ngrams)

    sim = len(set1.intersection(set2))/len(set1.union(set2))

    return sim

def getSubstringSim(str1, str2):
    s = dl.SequenceMatcher(None, str1, str2)
    res = s.find_longest_match(0,len(str1),0,len(str2))
    matching_string = str1[res.a:res.a+res.size]

    return len(matching_string) / len(str2), matching_string
    #return len(matching_string)/max(len(str1), len(str2)), matching_string

def getDistSim(str1, str2):
    X = Counter(str1)
    Y = Counter(str2)
    Xs = set(X.keys())
    Ys = set(Y.keys())
    tot_sum = 0

    for c in Xs.intersection(Ys):
        smaller = min(X[c], Y[c])
        larger = max(X[c], Y[c])
        tot_sum += smaller/larger

    return tot_sum/len(Xs.union(Ys))

def mul_dict(dict1, dict2):
    dim_names = set(dict1.keys()).union(set(dict2.keys()))

    total = 0
    for component in dim_names:
        if(component not in dict1.keys() or component not in dict2.keys()):
            total += 0
        else:
            total += dict1[component] * dict2[component]

    return total

def getCosineSimilarity(dict1, dict2):
    AB = mul_dict(dict1, dict2)
    AA = mul_dict(dict1, dict1)
    BB = mul_dict(dict2, dict2)

    sim = AB/(AA**0.5 * BB**0.5)
    #dict1_s = pd.Series(dict1)
    #dict2_s = pd.Series(dict2)

    return sim
    #return 1 - cosine(dict1_s, dict2_s)

def getJaccardSim(str1, str2, option):
    a = set()
    b = set()

    if(option == "w"):
        a = set(str1.split(" "))
        b = set(str2.split(" "))
    elif (option == "l"):
        a = set(str1)
        b = set(str2)
    else:
        a = set(str1)
        b = set(str2)

    c = a.intersection(b)
    return float(len(c)) / (len(a) + len(b) - len(c))/(len(str1)+len(str2))**0.5

def getSubsetOfDataframe(df, ids):
    #mask = df['index'].isin(ids)
    #df = df[mask]

    df = df.loc[ids]

    return df

def getMAE(dict1, dict2):
    #CAREFUL, assuming that both contain the same keys!
    cumsum = 0
    for k in dict1.keys():
        cumsum += abs(dict1[k] - dict2[k])

    return cumsum/len(dict1)

def getRMAE(dict1, dict2):
    cumsum = 0
    for k in dict1.keys():
        cumsum += abs(dict1[k] - dict2[k])/dict1[k]

    return cumsum/len(dict1)

def matchIngToEquivClass(ing, equiv_dict):
    for k, s in equiv_dict.items():
        if(ing in s):
            return k

    return ing

def createNCharMapping(filename, n):
    nChar_map = defaultdict(list)

    with open(filename) as file:
        curr_row = 0
        for line in file:
            curr_row += 1

            if(curr_row == 1):
                continue

            row = line.split(";")
            long_desc = row[2]

            for j in range(0, len(long_desc), n): #maybe you can try with intersecting sets? hm or whole words!
                nChars = long_desc[j:j+n]
                nChar_map[nChars].append(line)

    output = ""
    i = 0
    for k,v in nChar_map.items():
        output += "key:"+str(k)+"\n"

        for l in v:
            output += l

        i+=1

        if(i%10 == 0):
            file = open("../../data/nutrient_info_bins.txt", "w+") #a bit too slow, output just the ids and then map ids to desc and nutrients (create your own class/object thingie)
            file.write(output)
            file.close()
            print(i)

    file = open("../../data/nutrient_info_bins.txt", "w+")
    file.write(output)
    file.close()

def parseNCharMapping():
    pass

def hasNumbers(string):
    return RE_D.search(string)

def allLegalChars(string):
    return set(string).issubset(legalChars)

def buildMeasurementSet(filename):
    measurementSet = set()

    with open(filename) as file:
        for line in file:
            arr = line.split(";")
            measurement = arr[0]
            count = arr[1]

            measurementSet.add(measurement)

    return measurementSet

def extractNouns(ing_name):
    noun_word = ""
    blob = tb.TextBlob(ing_name)

    for el in blob.tags:
        if(el[1] in ("NN", "NNS", "NNP", "NNPS")):
            noun_word += el[0] + " "

    return noun_word[0:len(noun_word)-1]

def extractDigits(word):
    return int(filter(str.isdigit, word))

def calculateSimilarityRowIngredients(ing_row, sampled_ing):
    row_dict = defaultdict(float)

    for ing in ing_row:
        arr = ing.split(";")
        row_dict[arr[2]] = float(arr[0])

    sim = getCosineSimilarity(sampled_ing, row_dict)

    return sim

def randomlySelectFromDict(dict_ing, ff, n, post_filtering, concurrent_path):
    #ff = filter function

    filteredDict = dict(filter(lambda elem: ff(elem[1]), dict_ing.items()))

    if(post_filtering == True):
        concurrent_dict = loadConcurrent(concurrent_path)
        post_filteredDict = {}

        sampled_ing = random.sample(filteredDict.keys(), 1)[0]
        intersected_ings = concurrent_dict[sampled_ing]

        for ing in filteredDict.keys():
            if(ing in intersected_ings):
                post_filteredDict[ing] = filteredDict[ing]
                #intersected_ings = intersected_ings.intersection(concurrent_dict[ing])

        filteredDict = post_filteredDict

    n = min(n, len(filteredDict))
    ings = random.sample(filteredDict.keys(), n)
    p = [1/n for i in range(n)]

    chosen = np.random.choice(list(ings), n, p=p, replace = False)

    ingredients = defaultdict(float)
    for ing in chosen:
        ingredients[ing] = filteredDict[ing]

    return ingredients

def loadIdToIngPairs(path):
    id_to_ing = {}

    with open(path) as f:
        for line in f:
            arr = line.split(":")
            id_to_ing[int(arr[0])] = arr[1].strip()

    return id_to_ing

def loadConcurrent(path):
    concurrent_map = defaultdict(set)

    with open(path) as f:
        for line in f:
            arr = line.split(":")
            ing_key = arr[0]

            ingredients = arr[1].split(";")
            for ing in ingredients:
                ing = ing.strip()
                if(ing != ""):
                    concurrent_map[ing_key].add(ing)

    return concurrent_map

def loadIndices(path):
    indices_set = set()

    with open(path) as f:
        for line in f:
            indices_set.add(int(line.strip()))

    return indices_set

def extractAllPermissibleMeasure(measures):
    measure_set = []

    for i in range(len(measures)):
        m = measures[i]
        measure_set.append((i, m.measure_name))

    return measure_set

def findMostSimilarMeasure(ingredient, measure):
    best_sim = -1
    best_match = None

    for i in ingredient:
        #sim = getJaccardSim(i.measure_name, measure, 'l')
        sim = getFreqOfMatchingNGrams(i.measure_name, measure, 3)

        if(sim == None):
            return None, 0

        if(sim > best_sim):
            best_match = i
            best_sim = sim

    return best_match, best_sim

def getIdsFromFile(filename):
    recipe_ids_set = list()

    with open(filename) as f:
        for id in f:
            recipe_ids_set.append(int(id))

    return recipe_ids_set

def estimateNumberOfServings(tallied_nutrients, actual_nutrients, nutrient_names):
    best_servings = -1
    best_division = tallied_nutrients
    best_dist = float("inf")

    for servings in range(1, 9):
        divided_nutrients = {nut:0 for nut in nutrient_names}

        for nut in nutrient_names:
            divided_nutrients[nut] = tallied_nutrients[nut]/servings

        dist = math.sqrt(sum((divided_nutrients[k] - actual_nutrients[k])**2 for k in nutrient_names)) #euclidean distance

        if(dist < best_dist):
            best_division = divided_nutrients
            best_servings = servings
            best_dist = dist

    return best_division, best_servings

def findGrams(ing_map, n_char_map, n, quantity, measure, ingr_name):
    #TODO: Check if this function works correctly, the ingredient being None sometimes is fishy hmm
    g_measures = ["g", "gram", "grams"]
    if(measure in g_measures):
        return quantity

    ing, _ = findInMap(ing_map, n_char_map, n, ingr_name)
    if(ing == None):
        return 0
    meas, _ = findMostSimilarMeasure(ing.nutr_info[idFromNutrient("calories")], measure)

    return meas.grams * quantity

def multiplyIngRow(row, mult_factor):
    modified_ings = []

    for ing in row.ingredients:
        quantity, measure, ing_name = ing.split(";")
        if (quantity == 'None'):
            continue
        quantity = float(quantity) * mult_factor
        modified_ings.append(str(quantity) + ";" + measure + ";" + ing_name)

    return modified_ings

def multiplyIngRowGrams(row, mult_factor):
    modified_ings = []

    for ing in row.ingredientsGrams:
        quantity, measure, ing_name = ing.split(";")
        if (quantity == 'None'):
            continue
        quantity = float(quantity) * mult_factor
        modified_ings.append(str(quantity) + ";" + measure + ";" + ing_name)

    return modified_ings

def multiplyNutrients(row, mult_factor, nutrient_list):
    multipliedNutrients = defaultdict(float)

    for nut in nutrient_list:
        multipliedNutrients[nut] = float(row[nut])*mult_factor

    return multipliedNutrients

def geneticMultiplyWholeRow(row, mult_factor, nutrient_list):
    multipliedNutrients = defaultdict(float)

    for nut in nutrient_list:
        multipliedNutrients[nut] = float(row[nut]) * mult_factor

    modified_ings = []

    for ing in row.ingredientsGrams:
        quantity, measure, ing_name = ing.split(";")
        if (quantity == 'None'):
            continue
        quantity = float(quantity) * mult_factor
        modified_ings.append(str(quantity) + ";" + measure + ";" + ing_name)

    for nut in nutrient_list:
        row.at[nut] = multipliedNutrients[nut]

    row.ingredientsGrams = modified_ings

    return row


def splitIngredientTerm(line, isIndexed):
    #will later change to split into search ingredient, cup, measure etc.
    #will have to do it smarter than this lol
    #check if second is a valid measure, cup, serving, slice, basically all measures that exist in the csv, if not, the entire thing is an ingredient name!!!!!!!

    ingr_arr = line.split(";")
    split_line = ingr_arr[0].split(" ")
    noWords = len(split_line)

    quantity = None
    measure = None
    ingr_name = None

    if (isIndexed == True):
        recipe_id = int(ingr_arr[1])
    else:
        recipe_id = -1

    #if(noWords < 2):
    #    return (None,)*4
    if(noWords == 2):
        quantity = split_line[0]
        ingr_name = split_line[1]
    else:
        quantity = split_line[0]

        if("egg" in split_line[1]):
            ingr_name = " ".join(split_line[1:])
        elif(hasNumbers(split_line[1])):
            if("/" in split_line[1]):
                quantity = split_line[1] + "+ (" + quantity + ")"
            else:
                quantity = split_line[1] + "* (" + quantity + ")"
            measure = split_line[2]
            ingr_name = " ".join(split_line[3:])
        else:
            measure = split_line[1]
            ingr_name = " ".join(split_line[2:])

    code = parser.expr(quantity).compile()
    quantity = eval(code)

    return quantity, measure, ingr_name, recipe_id

def smartSplitIngredientTerm(line, isIndexed):
    #TODO, check for measurements and extract them (get a list of measurements first), check for numbers at the start and extract those, the rest is ing. name
    measurementFile = "../data/measurements_unique.txt"
    measurementSet = buildMeasurementSet(measurementFile)

    quantity = None
    measure = None
    ingr_name = ""
    recipe_id = ""

    arr = line.split(";")

    line = arr[0]
    if(isIndexed):
        recipe_id = int(arr[1])
    else:
        recipe_id = -1

    line = line.replace("-", " ")
    line = line.replace("(", "")
    line = line.replace(")", "")
    words = line.split(" ")
    quantityExpr = ""
    pos = 0

    for word in words:
        word = word.lower()
        if(word == ''):
            continue

        if (pos < 2 and hasNumbers(word)):
            if(quantityExpr == ""):
                quantityExpr = "(" + word + ")"
            else:
                if("/" in word):
                    quantityExpr += "+ (" + word + ")"
                else:
                    quantityExpr += "* (" + word + ")" #use extractDigits if it gets stuck somewhere hm
        elif(pos < 7 and measure == None and word in measurementSet):
            measure = word
        else:
            ingr_name += word + " "

        pos += 1


    if((quantityExpr != "") and allLegalChars(quantityExpr)):
        code = parser.expr(quantityExpr).compile()
        quantity = eval(code)

    if(measure == "egg" or measure == "eggs"):
        ingr_name = measure
        measure = "large"

    if(len(arr) == 2 and measure == None):
        measure = "large"

    return quantity, measure, ingr_name.strip(), recipe_id


def createNCharIndex(ingredient_map, n): #if n = -1, map entire words
    nChar_map = defaultdict(list)

    for id, ing in ingredient_map.map_id_ing.items():
        table = str.maketrans(dict.fromkeys(string.punctuation))
        name_stripped = ing.name.translate(table)
        name_stripped = name_stripped.replace(" ", "")

        if(n == -1):
            words = name_stripped.split(" ")
            for w in words:  # maybe you can try with intersecting sets? hm or whole words!
                nChar_map[w].append(id)
        else:
            for j in range(0, len(name_stripped)-n+1):
            #for j in range(0, len(name_stripped), n):  # maybe you can try with intersecting sets? hm or whole words!
                nChars = name_stripped[j:j + n]
                nChar_map[nChars].append(id)

    return nChar_map

def findInMap(ingredient_map, n_char_map, n, search_string): #if n = -1, look for entire words
    #TODO test if changes to searching are actually better (removing spaces, searching from start to finish n-grams etc.)
    closest_ingredient = None
    closest_ing = None
    closest_similarity = -1
    search_string_nospace = ""

    if(n == -1):
        iter = search_string.split()
    else:
        search_string_nospace = search_string.replace(" ", "")
        iter = range(0, len(search_string_nospace)-n+1)
        #iter = range(0, len(search_string), n)

    for w in iter:  # maybe you can try with intersecting sets? hm or whole words!
        nChars = ""

        if(n == -1):
            nChars = w
        else:
            nChars = search_string_nospace[w:w+n]

        if(nChars not in n_char_map.keys()):
            continue;

        for ing_id in n_char_map[nChars]:
            ing_tmp = ingredient_map.map_id_ing[ing_id]

            cosine = strcmp.Cosine(3)
            p0 = cosine.get_profile(ing_tmp.name)
            p1 = cosine.get_profile(search_string)
            #sim = cosine.similarity(ing_tmp.name, search_string) #precompute profiles for additional speed-up?
            #sim = getJaccardSim(ing_tmp.name, search_string, "w")
            #sim, matching_string = getSubstringSim(ing_tmp.name, search_string)
            #sim = getFreqOfMatchingNGrams(ing_tmp.name, search_string, 3)
            sim = getDistSim(ing_tmp.name,search_string)
            #metric_lcs = strcmp.MetricLCS()
            #sim = metric_lcs.distance(ing_tmp.name, search_string)

            #plot_example(ing_tmp.name, search_string, lang="en")

            #P, R, sim = score([ing_tmp.name], [search_string], lang="en", model_type='bert-base-uncased')

            if(sim > closest_similarity):
                closest_ingredient = ing_tmp
                closest_similarity = sim


    return closest_ingredient, closest_similarity

#createNCharMapping("../data/food_nutrient_info_lowercase.csv", 3)