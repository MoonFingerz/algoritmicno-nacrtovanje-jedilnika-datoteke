from collections import defaultdict
import math
import random
import copy

import scripts.indexMap.helper_functions as hf

class Gene:
    def __init__(self, recipe):
        self.recipe = recipe

class GenerationUnit:
    def __init__(self, db, days, meals_per_day, nutrient_list):
        #initialise recipes from db by creating GeneticUnits

        self.days = days
        self.meals = meals_per_day

        self.gen_n = 0
        self.fitness = 0
        self.recipes = [[None for _ in range(meals_per_day)] for _ in range(days)]
        self.nutrient_list = nutrient_list

        sampled = self.generateSamples(db, days*meals_per_day)

        for i in range(days):
            for j in range(meals_per_day):
                self.recipes[i][j] = sampled[i*meals_per_day+j]

        #print("debug")

    def generateSamples(self, db, n):
        sampled = []
        db_samples = db.sample(n)

        for i in range(n):
            mult = random.uniform(3, 10)
            sampled.append(hf.geneticMultiplyWholeRow(db_samples.iloc[i], mult, self.nutrient_list))

        return sampled

    def mutate(self, db, mutate_chance):
        day_meal = random.randint(0, self.days * self.meals-1)
        day = day_meal // self.meals
        meal = day_meal % self.meals

        r = random.random()
        if (r < mutate_chance[0]):
            mult = random.uniform(0.5, 2)

            self.recipes[day][meal] = hf.geneticMultiplyWholeRow(self.recipes[day][meal], mult, self.nutrient_list)

        r = random.random()
        if (r < mutate_chance[1]):
            self.recipes[day][meal] = self.generateSamples(db, 1)[0]

        r = random.random()
        if (r < mutate_chance[2]):
            day_meal2 = random.randint(0, self.days * self.meals-1)
            day2 = day_meal2 // self.meals
            meal2 = day_meal2 % self.meals

            tmp = self.recipes[day][meal]
            self.recipes[day][meal] = self.recipes[day2][meal2]
            self.recipes[day2][meal2] = tmp

    def nutrient_score(self, daily_limits):
        total_nut_sum = defaultdict(int)
        err = 0

        for i in range(self.days):
            for j in range(self.meals):
                for nut in self.nutrient_list:
                    total_nut_sum[nut] += self.recipes[i][j][nut]

        for nut, quant in daily_limits.items():
            err += abs(quant*7 - total_nut_sum[nut])

        return math.sqrt(1/(err+1e-5))

    def preference_list_score(self, pref_list):
        score = 0
        pref_list_tally = copy.deepcopy(pref_list)

        for day in range(self.days):
            for meal in range(self.meals):
                ingredients = self.recipes[day][meal].ingredientsGrams

                for ing in ingredients:
                    arr = ing.split(";")

                    quant = float(arr[0])
                    ing_name = arr[2]

                    for ing_pref_list, pref_quant in pref_list_tally.items():
                        sim = hf.getFreqOfMatchingNGrams(ing_pref_list, ing_name, 3)
                        #sim, substr = hf.getSubstringSim(ing_pref_list, ing_name)

                        if(sim > 0.7):
                            pref_list_tally[ing_pref_list] -= quant
                            break

        for ing_pref_list, quant in pref_list_tally.items():
            #if(quant < 0):
            #    score += 1
            #else:
            #    score -= 1

            #if(quant > 50):
            #    score -= 1
            #else:
            #    score += 1-(min(abs(quant),300)/300) #right now it tends to get "exactly" the right amount, but perhaps you should strive to just use the ingredients up instead?

            if(quant < 0):
                score += 1
            #if(quant < -300):
            #   score += 0.5
            #elif(quant < 0):
            #    score += 1
            #elif(quant < 50):
            #    score += 0.5
            #else:
            #    score -= 1

        return score

    def repeat_score_freq(self):
        ing_count = defaultdict(int)
        score = 0
        recipe_id_set = defaultdict(set)
        recipe_title_set = defaultdict(set)

        for day in range(self.days):
            for meal in range(self.meals):
                recipe_id_set[day].add(self.recipes[day][meal]["index"])
                recipe_title_set[day].add(self.recipes[day][meal]["title"])
                for ingredients in self.recipes[day][meal].ingredientsGrams:
                    arr = ingredients.split(";")
                    ing_count[arr[2]] += 1

        #MAYBE YOU NEED TO SWAP THIS AROUND????????????????????? count > 3 not < 3
        for ing, count in ing_count.items():
            if(count >= 3):
                score -= 1
            else:
                score += 1

        for day in range(self.days-1):
            if(len(recipe_id_set[day].intersection(recipe_id_set[day+1])) > 0 or len(recipe_title_set[day].intersection(recipe_title_set[day+1])) > 0 ):
                #score -= 5
                return -500, 1

            if(len(recipe_id_set[day]) < 3 or len(recipe_title_set[day]) < 3):
                return -5000, 1

        return score, len(ing_count)

    def repeat_score(self):
        recipes_set = defaultdict(set)
        recipe_id_set = defaultdict(set)
        repeat_score = [0 for i in range(7)]

        for day in range(self.days):
            for meal in range(self.meals):
                for ingredients in self.recipes[day][meal].ingredientsGrams:
                    arr = ingredients.split(";")
                    recipes_set[day].add(arr[2])

                recipe_id_set[day].add(self.recipes[day][meal]["index"])

        #recipes_set[0] = set([1,2,3])
        #recipes_set[1] = set([8,9])
        #recipes_set[2] = set([3,4])
        #recipes_set[3] = set([6,7])
        #recipes_set[4] = set([8,9,1,2,3])
        #recipes_set[5] = set([1,2,3])
        #recipes_set[6] = set([1,2,3])

        for day1 in range(self.days):
            for day2 in range(self.days):
                if(day1 != day2):
                    repeat_score[day1] += len(recipes_set[day1].intersection(recipes_set[day2]))/len(recipes_set[day1].union(recipes_set[day2]))*((abs(day2 - day1)/6)-0.5)

        for day in range(self.days-1):
            if(len(recipe_id_set[day].intersection(recipe_id_set[day+1])) > 0 or len(recipe_id_set[day]) < 3):
                repeat_score[day] = -5

        return sum(repeat_score)#/len(repeat_score)

    def nutrient_score_weekly(self, daily_limits):
        day_sums = []
        day_scores = []

        for i in range(self.days):
            temp_sum = defaultdict(int)
            for j in range(self.meals):
                for nut in self.nutrient_list:
                    temp_sum[nut] += self.recipes[i][j][nut]

            day_sums.append(temp_sum)

        for i in range(self.days):
            for nut in self.nutrient_list:
                score = 1 - abs(day_sums[i][nut] - daily_limits[nut])/max(daily_limits[nut], day_sums[i][nut])

                if(day_sums[i][nut] > 1.5*daily_limits[nut]):
                    score = -5

                if(len(day_scores) <= i):
                    day_scores.append(score)
                else:
                    day_scores[i] += score

        return sum(day_scores)/len(day_scores)

    def assessFitness(self, pref_list, daily_limits, normalised=False, mult=(1,1,1)):
        #nutrient_score = self.nutrient_score(daily_limits)
        nutrient_score_weekly = self.nutrient_score_weekly(daily_limits)
        preference_score = self.preference_list_score(pref_list)
        repeat_score = self.repeat_score()
        no_ing = 1
        #repeat_score, no_ing = self.repeat_score_freq() # MAYBE YOU HAVE TO SWAP?????? IN REPEAT SCORE FREQ

        if(normalised == True):
            nutrient_score_weekly /= 7
            preference_score /= len(pref_list.keys())
            #repeat_score /= 1.2
            repeat_score /= no_ing

        nutrient_score_weekly *= mult[0]
        preference_score *= mult[1]
        repeat_score *= mult[2]

        self.fitness = nutrient_score_weekly + preference_score + repeat_score
        self.fitness_scores = (nutrient_score_weekly, preference_score, repeat_score)
        #self.fitness = 0.7*nutrient_score_weekly + 0.2*preference_score + 0.1*repeat_score