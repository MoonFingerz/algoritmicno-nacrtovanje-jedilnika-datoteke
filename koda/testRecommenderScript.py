import pandas as pd
import scripts.indexMap.ingredient as ing
import scripts.indexMap.helper_functions as hf
import scripts.indexMap.recommender as recommender
import time

USE_EQUIV_CLASSES = True #TODO write code for using equiv classes so it's easier to test later down the road

def TicTocGenerator():
    # Generator that returns time differences
    ti = 0           # initial time
    tf = time.time() # final time
    while True:
        ti = tf
        tf = time.time()
        yield tf-ti # returns the time difference

TicToc = TicTocGenerator() # create an instance of the TicTocGen generator

# This will be the main function through which we define both tic() and toc()
def toc(tempBool=True):
    # Prints the time difference yielded by generator instance TicToc
    tempTimeInterval = next(TicToc)
    if tempBool:
        return tempTimeInterval

def tic():
    # Records a time in TicToc, marks the beginning of a time interval
    toc(False)


#ALL RECOMMENDERS
#db_recipes_path = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal_sauces_desserts_removed.json" #don't forget to divide by noOfServings!!
#db_recipes_path = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal_sauces_desserts_titles_removed.json" #don't forget to divide by noOfServings!!
nutr_limit_path = "../data/recommender_files/limits_standard.txt"
preference_list_path = "../data/recommender_files/preference_list.txt"
nutrient_info_file = "../data/nutrients/epi_top600.csv"

#NNMF ONLY
#db_recipes_path = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes.json"
#db_recipes_path = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes_dividedByServings.json"
#db_recipes_path = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes_dividedByServings_dropped_weird_entries.json"
#db_recipes_path = "../data/dbs/epi_recipes_grams_final_normalised100kCal.json"
db_recipes_path = "../data/dbs/epi_recipes_grams_final_normalised100kCal_equiv_classes.json"

category = "all"
#matrix_path = "../data/dbs/matrix_" + category + "_equiv_classes"

#idToIng_path = "../results/id_ingredient_pair.txt"
#matrix_path = "../data/dbs/matrix_all_normalised_100kCal"

#idToIng_path = "../results/id_ingredient_pair_equiv_classes.txt"
idToIng_path = "../results/id_ingredient_pair_new_equiv.txt"
matrix_path = "../data/dbs/matrix_all_equiv_classes"

#idToIng_path = "../results/id_ingredient_pair_equiv_classes_removed_rare_ing.txt"
#matrix_path = "../data/dbs/matrix_dessert_equiv_classes"

filtering = True
tag = "all_normalised"

#db_recipes_path = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes_removed_rare_ing.json"
#matrix_path = "../data/dbs/matrix_equiv_classes_removed_rare_ing"
#idToIng_path = "../results/id_ingredient_pair_equiv_classes_removed_rare_ing.txt"

results_path = "../data/recommender_files/results"
concurrent_path = "../results/concurrent_ingredients.txt"

#GENETIC ALGORITHM
#db_recipes_path = "../data/dbs/epi_recipes_grams_final_normalised100kCal_equiv_classes_sauces_desserts_titles_removed.json"

N = 502 #number of generations
n = 50 #units per generation
mutate_chance = (0.3, 0.5, 0.2)
normalised = True
mult_factor = (2,1,1)
selection_opt = "crossoverTournament" #tournament, topN, crossoverTournament
#selection_opt = "topN"
#selection_opt = "tournament"
rec = recommender.Random("Random", nutr_limit_path, db_recipes_path, nutrient_info_file, preference_list_path)
tic()
rec.recommend()
print(toc())
#print(rec.score())
#rec.outputScores(1000)

#rec.type


#tic()
#rec = recommender.Heuristic("Heuristic", nutr_limit_path, db_recipes_path, nutrient_info_file, preference_list_path)
#rec.recommend(2, -1, 1/3, True)
#print(toc())

#mult_pref = 2 #DEFAULT: 2
#mult_nut = -1 #DEFAULT: -1 (means to use absolute error)
#mult_fat = 1/3 #DEFAULT: 1/3 (with which factor to multiply); set -1 if you do not wish to set a limit to fat
#rec.outputScores(5, mult_pref, mult_nut, mult_fat)
#rec.outputScores(100, 10, -1, 1/3, True)
#print(rec.score())

#mult_factor_list = [(1,1,1), (2,1,1), (3,1,1), (1,2,1), (1,3,1), (1,1,2), (1,1,3)]

#for m_fact in mult_factor_list:
#    for i in range(10):
#        rec = recommender.Genetic("Genetic", nutr_limit_path, db_recipes_path, nutrient_info_file, preference_list_path, N, n, mutate_chance, normalised, m_fact, selection_opt)
#        rec.recommend(tag="mfac"+str(i), out_it=100, output_to_file=True)

#for i in range(30):
#    rec = recommender.Genetic("Genetic", nutr_limit_path, db_recipes_path, nutrient_info_file, preference_list_path, N, n, mutate_chance, normalised, (1,1,1), selection_opt)
#    rec.recommend(tag=i, out_it=100, output_to_file=True)

#outF = open("../results/gen_times.txt", "w")

#for i in range(10, 500, 10):
#    tic()
#    rec = recommender.Genetic("Genetic", nutr_limit_path, db_recipes_path, nutrient_info_file, preference_list_path, i, n, mutate_chance, normalised, (1,1,1), selection_opt)
#    rec.recommend(tag="time", out_it=100, output_to_file=False)
#    outF.write(str(toc()) + ",")
    #print("Time: " + str(toc()))

#outF.close()
#rec = recommender.Genetic("Genetic", nutr_limit_path, db_recipes_path, nutrient_info_file, preference_list_path, N, n, mutate_chance, normalised, mult_factor, selection_opt)
#rec.recommend(tag="test", out_it=100, output_to_file=True)
#CHECK WHY NNMF HAS ONLY 1090 RECIPES IN DB????????????????????????????????????
#k_size=500;

rec = recommender.NNMF("NNMF", nutr_limit_path, db_recipes_path, nutrient_info_file, preference_list_path, matrix_path, results_path, idToIng_path, concurrent_path)
#rec.exportPreferenceList(opt = "default")
rec.recommend(post_filtering=True)

#rec.outputScore(tag, k_size, True, filtering)
#for i in range(99):
#    rec.outputScore(tag, k_size, False, filtering)

#day_id = -1
#meal_id = -1

#for day in range(rec.days):
#    for meal in range(rec.meals):
#        if(rec.weekly_plan[day][meal]["index"] == 18102):
#            day_id = day
#            meal_id = meal
#            break

#    if(day_id != -1):
#        break

#rec.replaceIngredient(day_id, meal_id, ["worcestershire sauce", "pepper", "clove", "chiles", "lemon juice", "butter", "oil"], 3) #TODO test after exporting db again to see if there's still 13kg of salt
#print(rec.weekly_plan[0][0].title, end= ": ")
#print(str(rec.weekly_plan[0][0].ingredientsGrams) + "\n")
#rec.replaceIngredient(0, 0, ["butter"], 5)