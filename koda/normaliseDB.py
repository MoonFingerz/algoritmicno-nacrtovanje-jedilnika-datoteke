import pandas as pd
import scripts.indexMap.helper_functions as hf

recipes_file = "../data/dbs/epi_recipes_top500_equipped.json"
recipes_file = "../data/dbs/epi_recipes_grams_final.json"
nutrient_names = ['carbohydrate', 'sugars', 'calories', 'sodium', 'protein', 'fat']

recipes = pd.read_json(recipes_file)
recipes.index = recipes["index"]
recipes[nutrient_names] = recipes[nutrient_names].astype(float)
i = 0

for index, row in recipes.iterrows():
    if (row.calories == 0):
        recipes.drop(index)
        continue
        #print("debug")

    mult_factor = float(100)/row.calories;

    for nut in nutrient_names:
        recipes.at[index, nut] = recipes.loc[index][nut]*mult_factor

    multipliedIngredients1 = hf.multiplyIngRow(row, mult_factor)
    recipes.at[index, 'ingredients'] = multipliedIngredients1

    multipliedIngredients2 = hf.multiplyIngRowGrams(row, mult_factor)
    recipes.at[index, 'ingredientsGrams'] = multipliedIngredients2

    i+=1

#recipes.reindex()
#recipes.to_json("../data/dbs/epi_recipes_top500_equipped_normalised100kCal.json")
recipes.to_json("../data/dbs/epi_recipes_grams_final_normalised100kCal.json")