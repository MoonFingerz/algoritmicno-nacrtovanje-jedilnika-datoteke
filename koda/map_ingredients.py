import pandas as pd
import difflib

#write a function or some way to extract ingredient quantity, measure and name

nut_info = pd.read_csv("../data/food_nutrient_info.csv", sep=";") # lines = reads file as json object per line
print(nut_info.Long_Desc[1])
i = 0

with open("../data/ingredientList.txt") as f:
    for line in f:
        ingr = line.split(";")

        ingr_name = ingr[0]
        recipe_id = int(ingr[1])

        for string in nut_info.Long_Desc:
            if "Butter" in string:
                s = difflib.SequenceMatcher(None, "Butter", string)
                #print(string + str(s.real_quick_ratio()))
        i += 1
        if(i%100 == 0):
            print(i)

#implement efficient structure for ingredientList
#use memoization
#maybe even run it in parallel?