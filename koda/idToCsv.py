import pandas as pd
import scripts.indexMap.helper_functions as hf

all_nutrients = pd.read_csv("../data/nutrients/ref.csv", sep=";")

ids = set()
topids = 200

with open("../data/nutrients/epi_top600_ids.txt") as f:
    for id in f:
        if(topids <= 0):
            break

        ids.add(int(id.strip()))
        topids -= 1


#all_nutrients = hf.getSubsetOfDataframe(all_nutrients, ids)
mask = all_nutrients['ndb_no'].isin(ids)
all_nutrients = all_nutrients[mask]

all_nutrients.to_csv("../data/nutrients/epi_top600.csv", sep=";", index=False)