import pandas as pd

#recipes_file = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal"
recipes_file = "../data/dbs/epi_recipes_grams_final_normalised100kCal_equiv_classes"
recipes = pd.read_json(recipes_file+".json")
recipes.index = recipes["index"]

all_categories = set()
banned_categories = set(["cupcake", "cookie", "chocolate", "ice cream", "sauce", "dessert", "frozen dessert", "cake", "vinaigrette", "drink", "drinks", "fruit juice", "alcoholic", "beer"])
banned_titles = set(["sauce", "cake", "cupcake", "cookie", "aioli", "vinegar", "seasoning", "dough", "champ", "jam", "vinaigrette", "punch", "eggnog", "guacamole", "rub", "dressing"])

for index, row in recipes.iterrows():
    categories = row.categories
    categories = set([x.lower() for x in categories])
    #all_categories = all_categories.union(set(categories))
    if(len(categories.intersection(banned_categories)) > 0):
        recipes.drop(index, inplace=True)
        continue

    for title in banned_titles:
        if(title in row.title.lower()):
            recipes.drop(index, inplace=True)
            break

print("debug")
#print(all_categories)

recipes.to_json(recipes_file+"_sauces_desserts_titles_removed.json")

