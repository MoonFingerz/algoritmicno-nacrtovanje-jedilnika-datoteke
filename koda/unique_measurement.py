from collections import defaultdict
import re

measurements_file = "../data/measurements.txt"
measurements = defaultdict(int)

with open(measurements_file) as f:
    for line in f:
        line = re.sub("\(.*\)", "", line)
        line = line.strip()

        if(line.endswith('s')):
            line = line[:-1]

        measurements[line] += 1
        measurements[line+"s"] += 1

with open("../data/measurements_unique.txt", "w") as text_file:
    #ingredientList.encode('utf-8')
    for i, c in measurements.items():
        text_file.write(i+";"+str(c)+"\n")
