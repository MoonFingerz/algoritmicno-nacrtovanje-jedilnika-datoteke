import pandas as pd
import scripts.indexMap.helper_functions as hf
from scripts.indexMap.ingredient import *

#matrix_path = "../data/dbs/matrix_equiv_classes_removed_rare_ing.json";
matrix_path = "../data/dbs/matrix_all_equiv_classes.json"

recipes = pd.read_json(matrix_path)
#recipes.index = recipes["index"]

i = 0; #remember that in Matlab, indices start with 1!

f = open("../results/id_ingredient_pair_new_equiv", "w")

for ing_name in recipes.columns.values:
    f.write(str(i)+":"+ing_name+"\n")
    i+=1

f.close()