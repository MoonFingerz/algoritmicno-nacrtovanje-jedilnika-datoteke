import pandas as pd
import scripts.indexMap.helper_functions as hf

recipes_path1 = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes.json"
recipes_path2 = "../data/dbs/full_format_recipes_pruned_nounised_wNoServings.json"

recipes = pd.read_json(recipes_path1)
recipes.index = recipes["index"]

no_people_recipes = pd.read_json(recipes_path2)
no_people_recipes.index = no_people_recipes["index"]

nutrients_to_mul = ["calories", "sodium", "protein", "fat", "carbohydrate", "sugars"]
nutrients_to_div = ["carbohydrate", "sugars"]

count = 0

for i, row in recipes.iterrows():
    totalSugars = 0

    for ing in row.ingredientsGrams:
        arr = ing.split(";")
        if ("sugar" in arr[2]):
            totalSugars += float(arr[0])

    if(totalSugars > row.sugars*1.1):
        recipes.drop(i, inplace=True)
        continue

        count += 1
        sugars_to_add = totalSugars - recipes.loc[i, "sugars"]

        #if(sugars_to_add < row.sugars*0.2):
        #    break

        recipes.at[i, "sugars"] += sugars_to_add*1.1
        recipes.at[i, "carbohydrate"] += sugars_to_add*1.1

        remainingCalories = row.calories - (row.fat * 9 + row.protein * 4)

        if (remainingCalories > 0):
            noPeople = max(1, round((row.carbohydrate*4)/remainingCalories))
        else:
            continue

        recipes.at[i, "sugars"] /= noPeople
        recipes.at[i, "carbohydrate"] /= noPeople

        #recipes.at[i, "calories"] += 4*totalSugars

        #mult_factor = float(100)/recipes.loc[i,"calories"]

        #for nut in nutrients_to_mul:
        #    recipes.at[i, nut] = recipes.loc[i][nut] * mult_factor
        #ing_grams = hf.multiplyIngRowGrams(row, mult_factor)
        #recipes.at[i, "ingredientsGrams"] = ing_grams

    #for nut in nutrients_to_div:
        #recipes.at[i, nut] /= noPeople

    print("debug")
    #recipes.at[i, "ingredientsGrams"] = []

#for i, row in recipes.iterrows():
#    remainingCalories = row.calories - (row.fat * 9 + row.protein * 4)

#    if(remainingCalories > 0):
#        noPeople = max(1, round((row.carbohydrate*4)/remainingCalories))
#    else:
#        continue

#    recipes.at[i, "sugars"] /= noPeople
#    recipes.at[i, "carbohydrate"] /= noPeople

print(count)
#recipes.to_json("../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes_dividedByServings.json")
recipes.to_json("../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes_dividedByServings_dropped_weird_entries.json")