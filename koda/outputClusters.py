import pandas as pd
from collections import defaultdict

recipes_path = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes.json"
clusters_path = "../results/clustering/clusters_indices_nutrients_Lovain_clustering.tab"

recipes = pd.read_json(recipes_path)
recipes.index = recipes["index"]

clusters = defaultdict(list)

f = open("../results/clustering/titles.tab", "w")

for i, row in recipes.iterrows():
    f.write(str(i) + "\t" + row.title)
    f.write("\n")

f.close()

with open(clusters_path) as f:
   for line in f:
       arr = line.split("\t")

       if(arr[0].isdigit()):
           clusters[arr[1].strip()].append(int(arr[0]))



for c, items in clusters.items():
    print(c, end=": ")
    for index in items:
        print(recipes.loc[index, "title"].strip(), end=", ")
    print("\n")




