import pandas as pd
import scripts.indexMap.helper_functions as hf
from scripts.indexMap.ingredient import *
from collections import defaultdict

recipes_path = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes.json"

recipes = pd.read_json(recipes_path)
recipes.index = recipes["index"]

recipe_to_pairs = defaultdict(set)

for index, row in recipes.iterrows():
    ings_together = set()
    for ing in row.ingredientsGrams:
        arr = ing.split(";")
        ings_together.add(arr[2])

    for ing in ings_together:
        recipe_to_pairs[ing] = recipe_to_pairs[ing].union(ings_together)

f = open("../results/concurrent_ingredients.txt", "w")
l = open("../results/list_of_less_than_frequent_ingredients.txt", "w")

for ing_name, conc_ing in recipe_to_pairs.items():
    f.write(ing_name+":")

    if(len(recipe_to_pairs[ing_name]) < 15):
        l.write(ing_name+"\n")

    for ing in conc_ing:
        f.write(ing+";")

    f.write("\n")

f.close()
l.close()