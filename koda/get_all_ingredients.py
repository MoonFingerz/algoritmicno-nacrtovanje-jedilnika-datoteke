import pandas as pd

allrecipes = "../data/dbs/allrecipes-recipes_pruned.json"
epi = "../data/dbs/full_format_recipes_pruned_nounised.json"

extract = "EPI" #options: ALLRECIPES, EPI

if(extract == "ALLRECIPES"):
    recipes = pd.read_json(allrecipes) # lines = reads file as json object per line

    ingredients = recipes[:]["ingredients"]
    ingredientList = ""

    for i in range(len(ingredients)):
        #ingredientArr = ingredients[i].split(",")

        for j in range(len(ingredients[i])):
            ingredientList += ingredients[i][j].strip().lower().replace(";",",").replace("\n", "")  + ";" + str(i)
            ingredientList += "\n"

        if(i%1000 == 0):
            print(i)

        #with open("ingredientList.txt", "w") as text_file:
        #   ingredientList.encode('utf-8')
        #   text_file.write(ingredientList)
    with open("../data/ingredients/allrecipes_ingredientList.txt", "w", encoding="utf-8") as text_file:
        ingredientList.encode('utf-8')
        text_file.write(ingredientList)
elif(extract == "EPI"):
    recipes = pd.read_json(epi)
    ingredients = recipes[:]["ingredients"]
    ingredientList = ""

    for i, value in ingredients.items():
        # ingredientArr = ingredients[i].split(",")

        for j in range(len(ingredients[i])):
            #ingredientList += ingredients[i][j].strip().lower().replace(";",",").replace("\n", "") + ";" + str(i)
            ingredientList += ingredients[i][j].strip().lower().replace("\n","")+";"+str(i)
            ingredientList += "\n"

        if (i % 1000 == 0):
            print(i)

    with open("../data/ingredients/epi_ingredientList_nounised.txt", "w", encoding="utf-8") as text_file:
        ingredientList.encode('utf-8')
        text_file.write(ingredientList)
