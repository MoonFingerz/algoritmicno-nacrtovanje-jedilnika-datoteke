filepath = '../data/measurements_unique.txt'
string = ""

with open(filepath) as fp:
   line = fp.readline()
   cnt = 1
   while line:
       measure = line.split(";")
       measure = measure[0]

       string += measure + " & "
       if(cnt % 50 == 0):
           string += measure + " \\\\ \n"

       line = fp.readline()
       cnt += 1

print(string)