import pandas as pd
import scripts.indexMap.helper_functions as hf
from scripts.indexMap.ingredient import *
from collections import defaultdict

import re

import random

recipes_path = "../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes.json"
ing_to_remove_path = "../results/list_of_less_than_frequent_ingredients.txt"

remove_ing_set = set()

with open(ing_to_remove_path) as f:
    for ing in f:
        remove_ing_set.add(ing.strip())


recipes = pd.read_json(recipes_path)
recipes.index = recipes["index"]

count_recipes_to_remove = 0

for index, row in recipes.iterrows():
    for ing in row.ingredientsGrams:
        arr = ing.split(";")

        if(arr[2] in remove_ing_set):
            count_recipes_to_remove += 1
            recipes.drop(index, inplace = True)
            break

recipes.to_json("../data/dbs/epi_recipes_top500_equipped_normalised100kCal_equiv_classes_removed_rare_ing.json")
print(count_recipes_to_remove)