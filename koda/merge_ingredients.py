from collections import defaultdict
import scripts.indexMap.helper_functions as hf

equivClasses = defaultdict(set)
isKey = True
key = ""


with open('../results/ingredients_sorted.txt') as f:
    for line in f:
        line = line.strip()

        #nouns = hf.extractNouns(line)
        if(isKey):
            key = line
            equivClasses[key].add(key)
            isKey = False
        else:
            sim, word = hf.getSubstringSim(line.strip(), key)
            if(sim > 0.6):
                equivClasses[key].add(line)
            else:
                key = line
                equivClasses[key].add(key)
                isKey = False


with open('../results/ingredients_equiv_classes.txt', 'w') as f:
    for k, v in equivClasses.items():
        f.write("%s;" % k)
        for equiv in v:
            f.write("%s," % equiv)

        f.write("\n")