import scripts.indexMap.helper_functions as hf
import textblob as tb
import pandas as pd

EXTRACT_OPTION = "EPI"
OPTIONS = "EPI\nnutrients\nALLRECIPES"

epi_ingredients_file = "../data/ingredients/epi_ingredientList"
allrecipes_ingredients_file =  "../data/ingredients/allrecipes_ingredientList"
nutrient_info_file = "../data/food_nutrient_info_lowercase.csv"
nounised = "../data/train_nounised_split.json"

ingredient_list_nouns = ""
i = 0

if(EXTRACT_OPTION == "ALLRECIPES"):
    with open(allrecipes_ingredients_file + ".txt", encoding="utf-8") as f:
        for line in f:
            quantity, measure, ing_name, recipe_id = hf.smartSplitIngredientTerm(line, True)
            nouns = hf.extractNouns(ing_name)

            ingredient_list_nouns += str(quantity) + ";" + str(measure) + ";" + nouns + ";" + str(recipe_id) + "\n"

            i+=1

            if(i%1000 == 0):
                print(i)

    with open(allrecipes_ingredients_file + "_nounised.txt", "w", encoding="utf-8") as text_file:
        text_file.write(ingredient_list_nouns)

elif(EXTRACT_OPTION == "nutrients"):
    nutrients = pd.read_csv(nutrient_info_file, sep=";")

    for i in range(0, len(nutrients.long_desc)):
        nouns = hf.extractNouns(nutrients.long_desc[i])
        nutrients.at[i, 'long_desc'] = nouns

        if(i%1000 == 0):
            print(i)

    nutrients.to_csv("../data/food_nutrient_info_nouns.csv", sep=";")

elif(EXTRACT_OPTION == "EPI"):
    #TODO rerun this script bcus I modified EGGS and also run it again after you change the csv ingredient names
    with open(epi_ingredients_file + ".txt", encoding="utf-8") as f:
        for line in f:
            ran = line.split(";")

            quantity, measure, ing_name, recipe_id = hf.smartSplitIngredientTerm(line, True)
            nouns = hf.extractNouns(ing_name)

            ingredient_list_nouns += str(quantity) + ";" + str(measure) + ";" + nouns + ";" + str(recipe_id) + "\n"

            i += 1

            if (i % 1000 == 0):
                print(i)

    with open(epi_ingredients_file + "_nounised.txt", "w", encoding="utf-8") as text_file:
        text_file.write(ingredient_list_nouns)

else:
    print("Enter a valid option:" + OPTIONS)
